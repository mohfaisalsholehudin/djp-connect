/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import "../../../../_metronic/_assets/sass/pages/login/classic/login-1.scss";
import { LayoutSplashScreen } from "../../../../_metronic/layout";
import { useSelector } from "react-redux";

import qs from "qs";
import axios from "axios";
import { connect } from "react-redux";
import * as auth from "../_redux/authRedux";

function AuthPage(props) {
  const { auth } = useSelector((state) => state.auth);
  const {LOCAL_URL, SSO_URL, MIDDLEWARE} = window.ENV;

  const str = "djpconnect:djpconnect";
  const pass = btoa(str);
  const [token, setToken] = useState("");

  const data = {
    grant_type: "authorization_code",
    code: props.code,
    // redirect_uri: `${REACT_APP_LOCAL_URL}/auth/login`,
    redirect_uri: `${LOCAL_URL}/auth/login`,
  };

  const config = {
    headers: {
      "Content-type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${pass}`,
    },
  };

  const dataClient = {
    clientId: "djpconnect",
    clientSecret: "djpconnect",
    token,
  };

  //After hit this function, you will get access_token
  const postCodeToSso = async () => {
    return await axios.post(
      `${SSO_URL}/oauth/token`,
      // `${REACT_APP_SSO_URL}/oauth/token`,
      qs.stringify(data),
      config
    );
  };

  //Check access_token to SSO, and you will get token IAM
  const checkToken = async () => {
    return await axios.post(
      `${SSO_URL}/oauth/check_token`,
      // `${REACT_APP_SSO_URL}/oauth/check_token`,
      qs.stringify(dataClient),
      config
    );
  };

  const [loading] = useState(true);

  useEffect(() => {
    const response = postCodeToSso();
    response
      .then(({ data: { access_token } }) => {
        setToken(access_token);
        //set token sso
        props.sso(access_token);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  useEffect(() => {
    if (auth !== undefined) {
      const response = checkToken();
      response
        .then(({ data, status }) => {
          if (status === 200) {
            const tok = data.authorities.filter(
              (data) => !data.includes("ROLE_")
            );
            props.getTokenIam(tok[0]);
            const headers = {
              "Content-Type": "application/json",
              Authorization: `Bearer ${tok[0]}`,
            };
            axios
              .post(`${MIDDLEWARE}/api/getwhoami`, null, { headers: headers })
              // .post(`${REACT_APP_MIDDLEWARE}/api/getwhoami`, null, { headers: headers })
              // .post(`/api/getwhoami`, null, { headers: headers })
              .then(({ data }) => {
                props.fulfillUser(data);
              })
              .catch((e) => {
                console.log(e);
              });
          }
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [auth]);

  return (
    <>
      {loading ? (
        <LayoutSplashScreen />
      ) : (
        <div className="d-flex flex-column flex-root">
          <div
            className="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white"
            id="kt_login"
          >
            <div
              className="flex-row-fluid d-flex flex-column position-relative p-7 overflow-hidden"
              style={{ backgroundColor: "#212C5F" }}
            >
              <div
                className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0"
                style={{ backgroundColor: "#212C5F" }}
              ></div>
              <div className="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5"></div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default withRouter(connect(null, auth.actions)(AuthPage));
