import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
// import { put, takeLatest } from "redux-saga/effects";
// import  {getUserByToken}  from "./authCrud";

export const actionTypes = {
  Login: "[Login] Action",
  Logout: "[Logout] Action",
  UserRequested: "[Request User] Action",
  UserLoaded: "[Load User] Auth API",
  ssoToken: "[SSO] Get Token",
  getUser: "[Get User] Action"
};

const initialAuthState = {
  //user_details
  user: undefined,
  //not_in_use
  authToken: undefined,
  //sso_token
  auth: undefined,
  //iam_token
  iamToken: undefined,
};

export const reducer = persistReducer(
  { storage, key: "djpconnect", whitelist: ["user", "auth","iamToken"] },
  (state = initialAuthState, action) => {
    switch (action.type) {
      case actionTypes.Login: {
        const { iamToken } = action.payload;
        return { ...state, iamToken};
      }
      // case actionTypes.Login: {
      //   const { authToken } = action.payload;
      //   return { ...state, authToken};
      // }

      case actionTypes.Logout: {
        // TODO: Change this code. Actions in reducer aren't allowed.
        return initialAuthState;
      }

      case actionTypes.ssoToken: {
        const {auth} = action.payload;
        return {...state, auth}
      }
      case actionTypes.getUser: {
        const {url} = action.payload;
        return {...state, url}
      }

      case actionTypes.UserLoaded: {
        const { user } = action.payload;
        return { ...state, user };
      }
      default:
        return state;
    }
  }
);

export const actions = {
  // login: authToken => ({ type: actionTypes.Login, payload: { authToken } }),
  logout: () => ({ type: actionTypes.Logout }),
  requestUser: user => ({ type: actionTypes.UserRequested, payload: { user } }),
  fulfillUser: user => ({ type: actionTypes.UserLoaded, payload: { user } }),
  // GetUser: url => ({type: actionTypes.getUser, payload: { url }}),
  sso: auth => ({type: actionTypes.ssoToken, payload: {auth}}),
  getTokenIam: iamToken => ({ type: actionTypes.Login, payload: { iamToken } })
};


export function* saga() {
  // yield takeLatest(actionTypes.Login, function* loginSaga() {
  //   yield put(actions.requestUser());
  // });

  // yield takeLatest(actionTypes.UserRequested, function* userRequested() {
  //   const { data } = yield getUserByToken();

  //   yield put(actions.fulfillUser(data));
  // });

}
