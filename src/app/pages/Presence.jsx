/* eslint-disable no-use-before-define */
import React, { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";

function Presence() {
  const { user } = useSelector((state) => state.auth);
  const [presence, setPresence] = useState([]);
  const nip = user.pegawai.nip9;
  const not = "ANDA TIDAK MELAKUKAN PRESENSI";
  const notYet = "ANDA BELUM MELAKUKAN PRESENSI";
  const { MIDDLEWARE } = window.ENV;

  useEffect(() => {
    getPresenceToday();
  }, []);

  const getPresenceToday = useCallback(() => {
    axios
      .post(`${MIDDLEWARE}/api/getlogpresensi`, { nip })
      // .post(`/api/getlogpresensi`, { nip })
      .then(({ data }) => {
        if (data.length === 0) {
          const date = new Date();
          const tgl =
            String(date.getDate()).padStart(2, "0") +
            "-" +
            String(date.getMonth() + 1).padStart(2, "0") +
            "-" +
            date.getFullYear();
          setPresence([{ TANGGAL_ABSEN: tgl, MASUK: null, KELUAR: null }]);
        } else {
          setPresence(data);
        }

        getLastPresence().then((add) => {
          if (add.data.length === 0) {
            const i = 4;
            const fill = [];
            for (var x = 0; x < i; x++) {
              const b = { TANGGAL_ABSEN: null, MASUK: null, KELUAR: null };
              fill.push(b);
            }
            setPresence((presence) => [...presence, fill]);
          } else if (add.data.length < 4 && add.data.length > 0) {
            const data = add.data;
            const dt = new Date(data[add.data.length - 1].MASUK);

            const i = 4 - add.data.length;
            for (var y = 0; y < i; y++) {
              dt.setDate(dt.getDate() - 1);
              const day = String(dt.getDate()).padStart(2, "0");
              const mon = String(dt.getMonth() + 1).padStart(2, "0");
              const year = dt.getFullYear();
              const tgl = day + "-" + mon + "-" + year;
              const b = { TANGGAL_ABSEN: tgl, MASUK: null, KELUAR: null };
              data.push(b);
            }

            setPresence((presence) => [...presence, data]);
          } else {
            setPresence((presence) => [...presence, add.data]);
          }
        });
      });
  }, []);

  const getLastPresence = useCallback(() => {
    // return axios.post(`/api/getlastabsen`, { nip });
    return axios.post(`${MIDDLEWARE}/api/getlastabsen`, { nip });
  }, [nip]);

  const checkIn = (status, data, index) => {
    if (status === "parent") {
      if (data === null) {
        if (index > 0) {
          return (
            <span
              className="font-weight-bolder d-block font-size-md"
              style={{ color: "#BE1E2D" }}
            >
              {not} DATANG
            </span>
          );
        } else {
          return (
            <span
              className="font-weight-bolder d-block font-size-md"
              style={{ color: "#F7941D" }}
            >
              {notYet} DATANG
            </span>
          );
        }
      } else {
        return (
          <span
            className="font-weight-bolder d-block font-size-md"
            style={{ color: "#009444" }}
          >
            {data.slice(11)}
          </span>
        );
      }
    } else {
      if (data === null) {
        return (
          <span
            className="font-weight-bolder d-block font-size-md"
            style={{ color: "#BE1E2D" }}
          >
            {not} DATANG
          </span>
        );
      } else {
        return (
          <span
            className="font-weight-bolder d-block font-size-md"
            style={{ color: "#009444" }}
          >
            {data.slice(11)}
          </span>
        );
      }
    }
  };

  const checkOut = (status, data, index) => {
    if (status === "parent") {
      if (data === null) {
        if (index > 0) {
          return (
            <span
              className="font-weight-bolder d-block font-size-md"
              style={{ color: "#BE1E2D" }}
            >
              {not} PULANG
            </span>
          );
        } else {
          return (
            <span
              className="font-weight-bolder d-block font-size-md"
              style={{ color: "#F7941D" }}
            >
              {notYet} PULANG
            </span>
          );
        }
      } else {
        return (
          <span
            className="font-weight-bolder d-block font-size-md"
            style={{ color: "#009444" }}
          >
            {data.slice(11)}
          </span>
        );
      }
    } else {
      if (data === null) {
        return (
          <span
            className="font-weight-bolder d-block font-size-md"
            style={{ color: "#BE1E2D" }}
          >
            {not} PULANG
          </span>
        );
      } else {
        return (
          <span
            className="font-weight-bolder d-block font-size-md"
            style={{ color: "#009444" }}
          >
            {data.slice(11)}
          </span>
        );
      }
    }
  };

  const checkDate = (status, data, index) => {
    if (status === "parent") {
      if (data !== null) {
        if (data.length === 10) {
          return data;
        } else {
          const raw = data;
          const date = [raw.slice(6), raw.slice(4, 6), raw.slice(0, 4)].join(
            "-"
          );
          return date;
        }
      } else {
        const yesterday = new Date(Date.now() - 86400000);
        return (
          String(yesterday.getDate()).padStart(2, "0") +
          "-" +
          String(yesterday.getMonth() + 1).padStart(2, "0") +
          "-" +
          yesterday.getFullYear()
        );
      }
    } else {
      if (data === null) {
        const i = index + 1;
        const yesterday = new Date(Date.now() - 86400000 * i);
        return (
          String(yesterday.getDate()).padStart(2, "0") +
          "-" +
          String(yesterday.getMonth() + 1).padStart(2, "0") +
          "-" +
          yesterday.getFullYear()
        );
      } else {
        if (data.length === 10) {
          return data;
        } else {
          const raw = data;
          const date = [raw.slice(6), raw.slice(4, 6), raw.slice(0, 4)].join(
            "-"
          );
          return date;
        }
      }
    }
  };

  const showPresence = () => {
    return presence.map((data, index) => {
      if (data.length > 1) {
        return data.map((data, index) => {
          return (
            <tr key={index}>
              <td className="pl-0">
                <div className="d-flex align-items-center">
                  <div className="symbol symbol-40 symbol-light mr-4">
                    <span className="symbol-label">
                      <i
                        className="flaticon-calendar-with-a-clock-time-tools text-warning"
                        style={{ fontSize: "1.7rem" }}
                      ></i>
                    </span>
                  </div>
                  <div>
                    <span className="text-dark-75 font-weight-bolder d-block font-size-md">
                      {checkDate("child", data.TANGGAL_ABSEN, index)}
                    </span>
                  </div>
                </div>
              </td>
              <td className="text-center">
                {checkIn("child", data.MASUK, index)}
              </td>

              <td className="text-center">
                {checkOut("child", data.KELUAR, index)}
              </td>
            </tr>
          );
        });
      } else {
        return (
          <tr key={index}>
            <td className="pl-0">
              <div className="d-flex align-items-center">
                <div className="symbol symbol-40 symbol-light mr-4">
                  <span className="symbol-label">
                    <i
                      className="flaticon-calendar-with-a-clock-time-tools text-warning"
                      style={{ fontSize: "1.7rem" }}
                    ></i>
                  </span>
                </div>
                <div>
                  <span className="text-dark-75 font-weight-bolder d-block font-size-md">
                    {checkDate("parent", data.TANGGAL_ABSEN, index)}
                  </span>
                </div>
              </div>
            </td>
            <td className="text-center">
              {checkIn("parent", data.MASUK, index)}
            </td>

            <td className="text-center">
              {checkOut("parent", data.KELUAR, index)}
            </td>
          </tr>
        );
      }
    });
  };

  return (
    <>
      <div className="tab-content">
        <div className="table-responsive">
          <table className="table table-head-custom table-head-bg table-borderless table-vertical-center">
            <thead>
              <tr className="text-left text-uppercase">
                <th
                  className="pl-20"
                  style={{ minWidth: "150px" }}
                  ref={(el) => {
                    if (el) {
                      el.style.setProperty("color", "#1D428A", "important");
                    }
                  }}
                >
                  Tanggal
                </th>
                <th
                  style={{ minWidth: "100px", textAlign: "center" }}
                  ref={(el) => {
                    if (el) {
                      el.style.setProperty("color", "#1D428A", "important");
                    }
                  }}
                >
                  Presensi Masuk
                </th>
                <th
                  style={{ minWidth: "100px", textAlign: "center" }}
                  ref={(el) => {
                    if (el) {
                      el.style.setProperty("color", "#1D428A", "important");
                    }
                  }}
                >
                  Presensi Keluar
                </th>
              </tr>
            </thead>
            <tbody>{showPresence()}</tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default Presence;
