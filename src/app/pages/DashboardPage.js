/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { Carousel, Card } from "react-bootstrap";
import { toAbsoluteUrl } from "../../_metronic/_helpers";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from "axios";

import Presence from "./Presence";
import News from "./News";
import Sak from "./Sak";

export function DashboardPage() {
  const history = useHistory();
  const { iamToken } = useSelector((state) => state.auth);
  const {MIDDLEWARE} = window.ENV;

  const checkStatus = async () => {
    try {
      const config = {
        "Content-type": "application/json",
        Authorization: `Bearer ${iamToken}`,
      };
      const data = {};
      const response = await axios.post(`${MIDDLEWARE}/api/getwhoami`, {data: data}, {
      // const response = await axios.post(
        // `/api/getwhoami`,
        // { data: data },
        // {
          headers: config,
        }
      );
      return response;
    } catch (err) {
      history.push('/logout');
      // if (err.response) {
      //   if (err.response.status === 401) {
      //     history.push("/logout");
      //   } else {
      //     console.log(err.response.status);
      //   }
      // } else {
      //   console.log(err);
      // }
    }
  };

  useEffect(() => {
    checkStatus();
    return () => {
      checkStatus();
    };
  }, [history]);

  //how to check to Local Storage
  // useEffect(() => {
  //   const a = localStorage.getItem('persist:demoApp');
  //   const user = JSON.parse(a);
  //   console.log(user.authToken);
  // },[])

  return (
    <>
      {/* <div className="container-fluid"> */}
      {/* <div className="row">
          <div className="col-lg-8">
            <Form>
              <Form.Group>
                <Form.Control
                  type="text"
                  placeholder="DJPSearch"
                  style={{
                    textAlign: "end",
                    width: "450px",
                    height: "40px",
                    borderRadius: "50px",
                    backgroundColor: "#F2F2F2",
                    fontWeight: "600",
                    marginLeft: "45%"
                  }}
                />
              </Form.Group>
            </Form>
          </div>
          <div className="col-lg-4">
            <div
              className="btn btn-icon w-auto align-items-center btn-lg px-2"
              onClick={() => alert("halo")}
              style={{ marginLeft: "-20px" }}
            >
              <img
                width="50px"
                height="50px"
                src={toAbsoluteUrl("/media/logos/djp-search.png")}
                alt="images"
              />
            </div>
          </div>
        </div> */}
      <div className="row">
        {/* <div className="col-lg-12"> */}
        <div
          className="col-lg-12 col-xxl-12 order-1 order-xxl-1"
          style={{ paddingBottom: "20px" }}
        >
          <Carousel>
            <Carousel.Item>
              <img
                // width="800"
                height="300"
                className="d-block w-100"
                src={toAbsoluteUrl("/media/stock-900x600/img1.jpg")}
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                // width="800"
                height="300"
                className="d-block w-100"
                src={toAbsoluteUrl("/media/stock-900x600/img2.jpg")}
                alt="Third slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                // width="800"
                height="300"
                className="d-block w-100"
                src={toAbsoluteUrl("/media/stock-900x600/img3.jpg")}
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
        </div>
        {/* <div
          className="col-lg-6 col-xxl-4 order-1 order-xxl-2"
          style={{ paddingBottom: "20px" }}
        >
          <Card
            style={{
              borderLeft: "10px solid #FFC91B",
              borderRight: "10px solid #FFC91B",
              borderBottom: "10px solid #FFC91B",
              borderTop: "1px solid #FFC91B"
            }}
          >
            <Card.Header
              style={{
                backgroundColor: "#FFC91B",
                fontWeight: "700",
                borderRadius: "0px"
              }}
            >
              Meja Kerjaku
            </Card.Header>
            <Card.Body>
              <div className="mb-10">
                <div className="d-flex align-items-center">
                  <i
                    className="flaticon2-file mr-4"
                    style={{ color: "#1D428A", fontSize: "2rem" }}
                  ></i>
                  <div className="d-flex flex-column flex-grow-1">
                    <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">
                      Seksi Pengembangan Sistem Pendukung Manajemen
                    </span>
                    <span className="text-muted font-weight-bold">
                      08/02/2021, 20:50:08
                    </span>
                  </div>
                </div>
                <p className="text-dark-50 m-0 pt-5 font-weight-normal">
                  Pembahasan Media Sosial Internal
                </p>
              </div>
              <div className="mb-10">
                <div className="d-flex align-items-center">
                  <i
                    className="flaticon2-file mr-4"
                    style={{ color: "#1D428A", fontSize: "2rem" }}
                  ></i>
                  <div className="d-flex flex-column flex-grow-1">
                    <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">
                      Direktorat Teknologi Informasi dan Komunikasi
                    </span>
                    <span className="text-muted font-weight-bold">
                      18/01/2021, 18:14:09
                    </span>
                  </div>
                </div>
                <p className="text-dark-50 m-0 pt-5 font-weight-normal">
                  Undangan Rapat UAT untuk Aplikasi SIKEU
                </p>
              </div>
              <div className="mb-6">
                <div className="d-flex align-items-center flex-grow-1">
                  <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
                    <span
                      className="btn btn-sm btn-light-primary btn-inline font-weight-bold py-4"
                      onClick={() => history.push('/mytask')}
                    >
                      Lihat Lainnya . . .
                    </span>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </div> */}
        {/* <div
          className="col-lg-6 col-xxl-4 order-1 order-xxl-2"
          style={{ paddingBottom: "20px" }}
        >
          <Card
            style={{
              borderLeft: "10px solid #1D428A",
              borderRight: "10px solid #1D428A",
              borderBottom: "10px solid #1D428A",
              borderTop: "1px solid #1D428A"
            }}
          >
            <Card.Header
              style={{
                backgroundColor: "#1D428A",
                fontWeight: "700",
                borderRadius: "0px",
                color: "#ffffff"
              }}
            >
              Notifikasi
            </Card.Header>
            <Card.Body>
              <div className="mb-6">
                <div className="d-flex align-items-center flex-grow-1">
                  <i className="flaticon2-notification text-warning"></i>
                  <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
                    <div className="d-flex flex-column align-items-cente py-2 w-75">
                      <span className="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1 ml-3">
                        Zoom Meeting with Stakeholder
                      </span>
                    </div>
                    <span className="label label-lg label-light-success label-inline font-weight-bold py-4">
                      Success
                    </span>
                  </div>
                </div>
              </div>
              <div className="mb-6">
                <div className="d-flex align-items-center flex-grow-1">
                  <i className="flaticon2-notification text-warning"></i>
                  <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
                    <div className="d-flex flex-column align-items-cente py-2 w-75">
                      <span className="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1 ml-3">
                        Next Sprint Planning
                      </span>
                    </div>
                    <span className="label label-lg label-light-primary label-inline font-weight-bold py-4">
                      Approved
                    </span>
                  </div>
                </div>
              </div>
              <div className="mb-6">
                <div className="d-flex align-items-center flex-grow-1">
                  <i className="flaticon2-notification text-warning"></i>
                  <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
                    <div className="d-flex flex-column align-items-cente py-2 w-75">
                      <span className="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1 ml-3">
                        Data Analytic Research
                      </span>
                    </div>
                    <span className="label label-lg label-light-danger label-inline font-weight-bold py-4">
                      Rejected
                    </span>
                  </div>
                </div>
              </div>
              <div className="mb-6">
                <div className="d-flex align-items-center flex-grow-1">
                  <i className="flaticon2-notification text-warning"></i>
                  <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
                    <div className="d-flex flex-column align-items-cente py-2 w-75">
                      <span className="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1 ml-3">
                        Data Analytic Research
                      </span>
                    </div>
                    <span className="label label-lg label-light-danger label-inline font-weight-bold py-4">
                      Rejected
                    </span>
                  </div>
                </div>
              </div>
              
              <div className="mb-6">
                <div className="d-flex align-items-center flex-grow-1">
                  <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
                    <div className="d-flex flex-column align-items-cente py-2 w-75">
                        <span className="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1 ml-3">
                          Data Analytic Research
                        </span>
                      </div>
                    <span
                      className="btn btn-sm btn-light-primary btn-inline font-weight-bold py-4"
                      onClick={() => history.push('/notification')}
                    >
                      Lihat Lainnya . . .
                    </span>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </div> */}
        <div
          className="col-lg-12 col-xxl-12 order-1 order-xxl-2"
          style={{ paddingBottom: "20px" }}
        >
          <Card
            style={{
              borderLeft: "10px solid #FFC91B",
              borderRight: "10px solid #FFC91B",
              borderBottom: "10px solid #FFC91B",
              borderTop: "1px solid #FFC91B",
            }}
          >
            <Card.Header
              style={{
                backgroundColor: "#FFC91B",
                fontWeight: "700",
                borderRadius: "0px",
              }}
            >
              Pengumuman
            </Card.Header>
            <Card.Body>
              <News />
            </Card.Body>
          </Card>
        </div>
        <div
          className="col-lg-6 col-xxl-6 order-1 order-xxl-2"
          style={{ paddingBottom: "20px" }}
        >
          <Card
            style={{
              borderLeft: "10px solid #1D428A",
              borderRight: "10px solid #1D428A",
              borderBottom: "10px solid #1D428A",
              borderTop: "1px solid #1D428A",
            }}
          >
            <Card.Header
              style={{
                backgroundColor: "#1D428A",
                fontWeight: "700",
                borderRadius: "0px",
                color: "#ffffff",
              }}
            >
              Riwayat Presensi
            </Card.Header>
            <Card.Body>
              <Presence />
            </Card.Body>
          </Card>
        </div>
        <div
          className="col-lg-6 col-xxl-6 order-1 order-xxl-2"
          style={{ paddingBottom: "20px" }}
        >
          <Card
            style={{
              borderLeft: "10px solid #FFC91B",
              borderRight: "10px solid #FFC91B",
              borderBottom: "10px solid #FFC91B",
              borderTop: "1px solid #FFC91B",
            }}
          >
            <Card.Header
              style={{
                backgroundColor: "#FFC91B",
                fontWeight: "700",
                borderRadius: "0px",
              }}
            >
              Riwayat SAK
            </Card.Header>
            <Card.Body>
            <Sak />
            </Card.Body>
          </Card>
        </div>
      </div>
      <div className="row"></div>
      {/* </div> */}
    </>
  );
}
