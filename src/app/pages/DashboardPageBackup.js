/* eslint-disable react-hooks/exhaustive-deps */

import React, {useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { Modal, Button } from "react-bootstrap";
 

export function DashboardPage() {

  const {user, tokenIam} = useSelector(state => state.auth)
  const [role, setRole] = useState([]);
  const [pegawai, setPegawai] = useState([]);
  
  const [openModalPermission, setOpenModalPermission] = useState(false);
  const [openModalModul, setOpenModalModul] = useState(false);
  const [openModalApps, setOpenModalApps] = useState(false);
  const [permissionData, setPermissionData] = useState([]);
  const [modulData, setModulData] = useState([]);
  const [appsData, setAppsData] = useState([]);
  const [dataApps, setDataApps] = useState([]);



  const [modulBool, setModulBool] = useState(false);
  const [permBool, setPermBool] = useState(false);
  const [appsBool, setAppsBool] = useState(false)

  // const getRoles = async (roles) => {
   
  //     return await axios.get(`https://127.0.0.1:8000/api/roles?nama=${roles}`);
  // }
//  console.log(user.roles)
  const getDataPegawai = async () => {
    return await axios.get(`https://127.0.0.1:8000${user.pegawai['@id']}`)
  }


const perm = role.map((data)=>{
  return data.permissions
})

  useEffect(() => {
    // user.roles.map((roles) => {
    //   getRoles(roles)
    //   .then(({data}) => {
    //       setRole(data['hydra:member'])
    //   })
    //   .catch((e)=> {
    //     console.log(e)
    //   })
    // })
    getDataPegawai()
    .then(({data: {jabatanPegawais}})=> {
      setPegawai(jabatanPegawais)
    })
    .catch((e)=>{
      console.log(e)
    })

  }, []);


   const getRoles = async (roles) => {
   
       await axios.get(`https://127.0.0.1:8000/api/roles?nama=${roles}`)
       .then(({data})=> {
          setRole(data['hydra:member'])
       })
       .catch((e)=> {
         console.log(e)
       })
  }

 const getDataPermission = async (data) => {
     await axios.get(`https://127.0.0.1:8000${data}`)
      .then(({data})=>{
          setPermissionData(data)
          setModulData(data.modul)
          setModulBool(true)
      })
 }

 const getDataModul = async (data) => {
   await axios.get(`https://127.0.0.1:8000${data}`)
   .then(({data})=>{
      setAppsData(data)
      setAppsBool(true)

   })
 }

 const getDataApps = async (data) => {
   await axios.get(`https://127.0.0.1:8000${data}`)
   .then(({data})=>{
    setDataApps(data)
 })
 }


 const showRole =() => {
   return user.roles.map((data, index)=>(
        <div key={index} className="d-flex align-items-center mb-10">
          {/* <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4"> */}
              {/* <span></span> */}
              {/* </label> */}
            <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
              {/* <p key={data.id} className="text-dark text-hover-primary mb-1 font-size-lg">{data.nama}</p> */}
              <button aria-haspopup="true" aria-expanded="false" value={data} type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={(e)=>handleOpenRole(e)}>{data}</button>
           <br/>
            </div>
        </div> 
   ))
 }
 const showPermission = () => {

  return perm.map((data, index)=>
  {
    
    if(data.length === 0){
      return (
        <div key={index} className="d-flex align-items-center mb-10">
        {/* <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
           </label> */}
        <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
          {/* <p key={data} className="text-dark  mb-1 font-size-lg">{data}</p> */}
        <button aria-haspopup="true" aria-expanded="false"  type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 ">You don't have any permissions yet</button>
           <br/>
         </div>
   </div> 
      )
    }
    return(
    data.map((data, index)=>{
      return (
        <div key={index} className="d-flex align-items-center mb-10">
        {/* <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
           </label> */}
        <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
          {/* <p key={data} className="text-dark  mb-1 font-size-lg">{data}</p> */}
        <button aria-haspopup="true" aria-expanded="false" value={data} type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={(e)=>handleOpenModal(e)}>{data}</button>
           <br/>
         </div>
   </div> 
  
    )
    })
  )})
 }

 const showModul =() => {

 return perm.map((data, index)=> {
  if(data.length === 0){
    return (
      <div key={index} className="d-flex align-items-center mb-10">
      {/* <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
         </label> */}
      <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
        {/* <p key={data} className="text-dark  mb-1 font-size-lg">{data}</p> */}
      <button aria-haspopup="true" aria-expanded="false"  type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 ">You don't have any Modul yet</button>
         <br/>
       </div>
 </div> 
    )
  }
  return modulData.map((data, index)=>(
    <div key={index} className="d-flex align-items-center mb-10">
      {/* <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
          </label> */}
          <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
           <button aria-haspopup="true" aria-expanded="false" value={data} type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={(e)=>handleOpenModalModul(e)}>{data}</button>
           <br/>
      </div>
   </div> 
))

 })
  
}

const showApps = () => {
  
  return perm.map((data, index)=> {
    if(data.length === 0){
      return (
        <div key={index} className="d-flex align-items-center mb-10">
        {/* <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
           </label> */}
        <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
          {/* <p key={data} className="text-dark  mb-1 font-size-lg">{data}</p> */}
        <button aria-haspopup="true" aria-expanded="false"  type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 ">You don't have any Application yet</button>
           <br/>
         </div>
   </div> 
      )
    }


  return (
    <div key={appsData.id} className="d-flex align-items-center mb-10">
      {/* <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
          </label> */}
          <div key={appsData.id} className="d-flex flex-column flex-grow-1 font-weight-bold">
           <button aria-haspopup="true" aria-expanded="false" value={appsData.aplikasi} type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={(e)=>handleOpenModalApps(e)}>{appsData.aplikasi}</button>
           <br/>
      </div>
   </div> 
)
})
}



 const handleOpenModal = (e) => {
  getDataPermission(e.target.value);
  setOpenModalPermission(true);
 }

 const handleCloseModal = () => {
   setOpenModalPermission(false);
 }

 const handleOpenRole = (e) => {
  getRoles(e.target.value);
  setPermBool(true);
  // setOpenModalPermission(true);
 }


 const handleOpenModalModul = (e) => {
  getDataModul(e.target.value);
  setOpenModalModul(true);
 }

 const handleCloseModalModul = () => {
   setOpenModalModul(false);
 }

 const handleOpenModalApps = (e) => {
  getDataApps(e.target.value);
  setOpenModalApps(true);
 }

 const handleCloseModalApps = () => {
   setOpenModalApps(false);
 }

 const showJabatanPegawai = () => {
   return pegawai.map((data, index)=> (
    <div key={index} className="py-9">
    <div key={data.jabatan['@id']}  className="d-flex align-items-center justify-content-between mb-2">
      <span className="font-weight-bold mr-2">Nama Jabatan:</span>
      <span className="text-muted text-hover-primary">{data.jabatan.nama}</span>
    </div>
    <div key={data.jabatan['@type']} className="d-flex align-items-center justify-content-between mb-2">
      <span className="font-weight-bold mr-2">Jenis Jabatan:</span>
      <span className="text-muted text-hover-primary">{data.jabatan.jenis}</span>
    </div>
    <div key={data.kantor['@id']} className="d-flex align-items-center justify-content-between mb-2">
      <span className="font-weight-bold mr-2">Nama Kantor:</span>
      <span className="text-muted text-hover-primary">{data.kantor.nama}</span>
    </div>
    <div key={data.unit['@id']} className="d-flex align-items-center justify-content-between mb-2">
      <span className="font-weight-bold mr-2">Unit Organisasi:</span>
      <span className="text-muted text-hover-primary">{data.unit.nama}</span>
    </div>
  
  </div>
   ))
 }

 
  return (
    <div className="container">
    <div className="row">
    <div className="col-xxl-6 order-2 order-xxl-1">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Detail User</h3>
          </div>
          <div className="card-body pt-2">
          <div className="py-9">
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Nama:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.nama}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">IP SIKKA:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.nip9}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">NIP:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.nip18}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Tanggal Lahir:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.tanggalLahir}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Tempat Lahir:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.tempatLahir}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Jenis Kelamin:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.jenisKelamin.nama}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Agama:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.agama.nama}</span>
            </div>
          </div>
          </div>
           
        </div>

      </div>
      <div className="col-xxl-6 order-2 order-xxl-1">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Jabatan Pegawai</h3>
          </div>
          <div className="card-body pt-2">
            {showJabatanPegawai()}
          </div>
           
        </div>

      </div>


      <div className="col-lg-6 col-xxl-3 order-1 order-xxl-1">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Roles</h3>
          </div>
          <div className="card-body pt-2">
          {showRole()}
          </div>
           
        </div>

      </div>

      {permBool? <div className="col-lg-6 col-xxl-3 order-1 order-xxl-2">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Permission</h3>
          </div>
          <div className="card-body pt-2">
          {showPermission()}
          </div>
          
        </div>

      </div> : ''}
      
      
      {modulBool? <div className="col-lg-6 col-xxl-3 order-1 order-xxl-2">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Modul</h3>
          </div>
          <div className="card-body pt-2">
          {showModul()}
          </div>
          
        </div>

      </div> : ''}
      {appsBool? <div className="col-lg-6 col-xxl-3 order-1 order-xxl-2">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Applications</h3>
          </div>
          <div className="card-body pt-2">
          {showApps()}
          </div>
          
        </div>

      </div> : ''}
      
      

    </div>


    <Modal show={openModalPermission} onHide={()=>handleCloseModal()}>
        <Modal.Header closeButton>
        <Modal.Title>Permission Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="row">
                <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
                Permission Name: 
                </label>
                  <div  className="d-flex flex-column flex-grow-1 font-weight-bold">
                    <p className="text-dark  mb-1 font-size-lg">{permissionData.nama}</p>
                  </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={()=>handleCloseModal()}>
            Close
          </Button>
          
        </Modal.Footer>
    </Modal>
    <Modal show={openModalModul} onHide={()=>handleCloseModalModul()}>
        <Modal.Header closeButton>
        <Modal.Title>Modul Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="row">
                <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
                Modul Name: 
                </label>
                  <div  className="d-flex flex-column flex-grow-1 font-weight-bold">
                    <p className="text-dark  mb-1 font-size-lg">{appsData.nama}</p>
                  </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={()=>handleCloseModalModul()}>
            Close
          </Button>
          
        </Modal.Footer>
    </Modal>
    <Modal show={openModalApps} onHide={()=>handleCloseModalApps()}>
        <Modal.Header closeButton>
        <Modal.Title>Application Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="row">
                <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
                Application Name: 
                </label>
                  <div  className="d-flex flex-column flex-grow-1 font-weight-bold">
                    <p className="text-dark  mb-1 font-size-lg">{dataApps.nama}</p>
                  </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={()=>handleCloseModalApps()}>
            Close
          </Button>
          
        </Modal.Footer>
    </Modal>
    
    
    </div>
  );
}

