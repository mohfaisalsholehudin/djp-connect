/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";

function Sak() {
  const { user } = useSelector((state) => state.auth);
  const nip = user.pegawai.nip9;
  const [sak, setSak] = useState([]);
  const { MIDDLEWARE } = window.ENV;

  useEffect(() => {
    getSak();
  }, []);

  const getSak = () => {
    // axios.post(`/api/getlogsak`, { nip })
      axios.post(`${MIDDLEWARE}/api/getlogsak`,{nip})
      .then(({ data }) => {
        setSak(data);
      });
  };

  const showSak = () => {
    return sak.map((data, index) => {
      const getDate = (data) => {
        const raw = data;
        const date = [raw.slice(8), raw.slice(5, 7), raw.slice(0, 4)].join("-");
        return date;
      };

      const checkStatus = (status) => {
        if (status === "Risiko Rendah") {
          return "success";
        } else if(status === "Kasus Sembuh") {
          return "success";
        }else if (status === "Kasus Konfirmasi Positif") {
          return "danger";
        } else if (status === "Positif"){
          return "danger";
        } else {
          return "warning";
        }
      };

      
      return (
        <tr key={index}>
          <td className="pl-0">
            <div className="d-flex align-items-center">
              <div
                className={`symbol symbol-40 symbol-light-${checkStatus(
                  data.status
                )} mr-4`}
              >
                <span className="symbol-label">
                  <i
                    className={`flaticon2-medical-records-1 text-${checkStatus(
                      data.status
                    )}`}
                    style={{ fontSize: "1.7rem" }}
                  ></i>
                </span>
              </div>
              <div>
                <span
                  className="text-dark-75 font-weight-bolder d-block font-size-md"
                  style={{ whiteSpace: "nowrap" }}
                >
                  {getDate(data.tanggal)}
                </span>
              </div>
            </div>
          </td>
          <td className="text-center" style={{ whiteSpace: "nowrap" }}>
            <span
              className={`label label-lg label-light-${checkStatus(
                data.status
              )} label-inline`}
            >
              {data.status}
            </span>
          </td>

          <td className="text-center">
            <span className="text-muted font-weight-bold d-block">
              {data.keterangan}
            </span>
          </td>
        </tr>
      );
    });
  };

  return (
    <>
      <div className="tab-content">
        <div className="table-responsive">
          <table className="table table-head-custom table-head-bg table-borderless table-vertical-center">
            <thead>
              <tr className="text-left text-uppercase">
                <th
                  className="pl-20"
                  style={{ minWidth: "100px" }}
                  ref={(el) => {
                    if (el) {
                      el.style.setProperty("color", "#F7941D", "important");
                    }
                  }}
                >
                  Tanggal
                </th>
                <th
                  style={{ minWidth: "100px", textAlign: "center" }}
                  ref={(el) => {
                    if (el) {
                      el.style.setProperty("color", "#F7941D", "important");
                    }
                  }}
                >
                  Status
                </th>
                <th
                  style={{ minWidth: "150px", textAlign: "center" }}
                  ref={(el) => {
                    if (el) {
                      el.style.setProperty("color", "#F7941D", "important");
                    }
                  }}
                >
                  Keterangan
                </th>
              </tr>
            </thead>
            <tbody>{showSak()}</tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default Sak;
