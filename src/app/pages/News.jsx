/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { useSelector } from "react-redux";


function News() {
  const history = useHistory();
  const [news, setNews] = useState([]);
  const [isMore, setIsMore] = useState(false);
  const { iamToken } = useSelector(state => state.auth);
  const {MIDDLEWARE} = window.ENV;


  

  useEffect(() => {
    getNews();
  }, [getNews]);

  const getNews = () => {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${iamToken}`
    }
    // axios.get("/api/getnews",{headers: headers})
    axios.get(`${MIDDLEWARE}/api/getnews`,{headers: headers})
    .then(({ data }) => {
      setNews(data["hydra:member"]);
      data["hydra:totalItems"] > 2 && setIsMore(true);
    });
  };

  const showNews = () => {
    return news.map((data, index) => {
      const date = new Date(data.createdAt);
      const created =
        String(date.getDate()).padStart(2, "0") +
        "/" +
        String(date.getMonth() + 1).padStart(2, "0") +
        "/" +
        date.getFullYear() +
        ", " +
        String(date.getHours()).padStart(2, "0") +
        ":" +
        String(date.getMinutes()).padStart(2, "0") +
        ":" +
        String(date.getSeconds()).padStart(2, "0");
      return (
        <div className="mb-10" key={index}>
          <div className="d-flex align-items-center">
            <i
              className="flaticon2-information text-primary mr-4"
              style={{ color: "#1D428A", fontSize: "2rem" }}
            ></i>
            <div className="d-flex flex-column flex-grow-1">
              <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">
                {data.title}
              </span>
              <span className="text-muted font-weight-bold">{created}</span>
            </div>
          </div>
          <p className="text-dark-50 m-0 pt-5 font-weight-normal">
            {data.content}
          </p>
        </div>
      );
    });
  };
  return (
    <>
      {showNews()}
      {isMore && (
        <div className="mb-6">
          <div className="d-flex align-items-center flex-grow-1">
            <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
              <span
                className="btn btn-sm btn-light-primary btn-inline font-weight-bold py-4"
                onClick={() => history.push("/announcement")}
              >
                See More . . .
              </span>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default News;
