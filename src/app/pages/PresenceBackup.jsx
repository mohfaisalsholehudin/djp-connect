/* eslint-disable no-use-before-define */
import React, { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { check } from "prettier";

function Presence() {
  const { user } = useSelector(state => state.auth);
  const [presence, setPresence] = useState([]);
  const nip = user.pegawai.nip9;
  const not = 'ANDA TIDAK MELAKUKAN PRESENSI';
  const notYet = 'ANDA BELUM MELAKUKAN PRESENSI';

  useEffect(() => {
    getPresenceToday();
  }, [getPresenceToday]);

  const getPresenceToday = useCallback(() => {
    axios
      .post("/logbook/public/rest/absensi/getlogpresensi", { nip })
      .then(({ data }) => {
        if (data.length === 0) {
          const date = new Date();
          const tgl =
            String(date.getDate()).padStart(2, "0") +
            "-" +
            String(date.getMonth() + 1).padStart(2, "0") +
            "-" +
            date.getFullYear();
          setPresence([{ TANGGAL_ABSEN: tgl, MASUK: null, KELUAR: null }]);
        } else {
          setPresence(data);
        }

        getLastPresence().then(add => {
          console.log(add.data.length);
          if (add.data.length === 0) {
            const yesterday = new Date(Date.now() - 86400000);
            const tgl =
              String(yesterday.getDate()).padStart(2, "0") +
              "-" +
              String(yesterday.getMonth() + 1).padStart(2, "0") +
              "-" +
              yesterday.getFullYear();

            setPresence(presence => [
              ...presence,
              [
                { TANGGAL_ABSEN: tgl, MASUK: null, KELUAR: null },
                { TANGGAL_ABSEN: null, MASUK: null, KELUAR: null }
              ]
            ]);
          } else if (add.data.length === 1) {
            const a = add.data;
            const data = [
              ...a,
              { TANGGAL_ABSEN: null, MASUK: null, KELUAR: null }
            ];
            setPresence(presence => [...presence, data]);
          } else {
            setPresence(presence => [...presence, add.data]);
          }
        });
      });
  }, [getLastPresence, nip]);

  const getLastPresence = useCallback(() => {
    return axios.post("/logbook/public/rest/absensi/getlastabsen", { nip });
  }, [nip]);

  // const showPresence = () => {
  //   return presence.map((data, index) => {
  //     const checkInParent = () => {
  //       if (data.MASUK === null) {
  //         if (index > 0) {
  //           return "ANDA TIDAK MELAKUKAN PRESENSI DATANG";
  //         }
  //         return "ANDA BELUM MELAKUKAN PRESENSI DATANG HARI INI";
  //       } else {
  //         return data.MASUK.slice(11);
  //       }
  //     };
  //     const checkOutParent = () => {
  //       if (data.KELUAR === null) {
  //         if (index > 0) {
  //           return "ANDA TIDAK MELAKUKAN PRESENSI PULANG";
  //         }
  //         return "ANDA BELUM MELAKUKAN PRESENSI PULANG HARI INI";
  //       } else {
  //         return data.KELUAR.slice(11);
  //       }
  //     };
  //     const checkDateParent = () => {
  //       if (data.TANGGAL_ABSEN !== null) {
  //         if (data.TANGGAL_ABSEN.length === 10) {
  //           return data.TANGGAL_ABSEN;
  //         } else {
  //           const tgl = data.TANGGAL_ABSEN;
  //           const gabung = [
  //             tgl.slice(6),
  //             tgl.slice(4, 6),
  //             tgl.slice(0, 4)
  //           ].join("-");
  //           return gabung;
  //         }
  //       } else {
  //         const yesterday = new Date(Date.now() - 86400000);
  //         return (
  //           String(yesterday.getDate()).padStart(2, "0") +
  //           "-" +
  //           String(yesterday.getMonth() + 1).padStart(2, "0") +
  //           "-" +
  //           yesterday.getFullYear()
  //         );
  //       }
  //     };
  //     if (data.length > 0) {
  //       return data.map((data, index) => {
  //         const checkDateChild = () => {
  //           if (data.TANGGAL_ABSEN === null) {
  //             const yesterday = new Date(Date.now() - 172800000);
  //             return (
  //               String(yesterday.getDate()).padStart(2, "0") +
  //               "-" +
  //               String(yesterday.getMonth() + 1).padStart(2, "0") +
  //               "-" +
  //               yesterday.getFullYear()
  //             );
  //           } else {
  //             if (data.TANGGAL_ABSEN.length === 10) {
  //               return data.TANGGAL_ABSEN;
  //             } else {
  //               const tgl = data.TANGGAL_ABSEN;
  //               const gabung = [
  //                 tgl.slice(6),
  //                 tgl.slice(4, 6),
  //                 tgl.slice(0, 4)
  //               ].join("-");
  //               return gabung;
  //             }
  //           }
  //         };

  //         const checkInChild = () => {
  //           if (data.MASUK === null) {
  //             return "ANDA TIDAK MELAKUKAN PRESENSI DATANG";
  //           } else {
  //             return data.MASUK.slice(11);
  //           }
  //         };
  //         const checkOutChild = () => {
  //           if (data.KELUAR === null) {
  //             return "ANDA TIDAK MELAKUKAN PRESENSI PULANG";
  //           } else {
  //             return data.KELUAR.slice(11);
  //           }
  //         };
  //         return (
  //           <tr key={index}>
  //             <td className="pl-0">
  //               <div className="d-flex align-items-center">
  //                 <div className="symbol symbol-40 symbol-light mr-4">
  //                   <span className="symbol-label">
  //                     <i
  //                       className="flaticon-calendar-with-a-clock-time-tools text-warning"
  //                       style={{ fontSize: "1.7rem" }}
  //                     ></i>
  //                   </span>
  //                 </div>
  //                 <div>
  //                   <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
  //                     {checkDateChild()}
  //                   </span>
  //                 </div>
  //               </div>
  //             </td>
  //             <td className="text-center">
  //               <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
  //                 {checkInChild()}
  //               </span>
  //             </td>

  //             <td className="text-center">
  //               <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
  //                 {checkOutChild()}
  //               </span>
  //             </td>
  //           </tr>
  //         );
  //       });
  //     } else {
  //       return (
  //         <tr key={index}>
  //           <td className="pl-0">
  //             <div className="d-flex align-items-center">
  //               <div className="symbol symbol-40 symbol-light mr-4">
  //                 <span className="symbol-label">
  //                   <i
  //                     className="flaticon-calendar-with-a-clock-time-tools text-warning"
  //                     style={{ fontSize: "1.7rem" }}
  //                   ></i>
  //                 </span>
  //               </div>
  //               <div>
  //                 <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
  //                   {checkDateParent()}
  //                 </span>
  //               </div>
  //             </div>
  //           </td>
  //           <td className="text-center">
  //             <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
  //               {checkInParent()}
  //             </span>
  //           </td>

  //           <td className="text-center">
  //             <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
  //               {checkOutParent()}
  //             </span>
  //           </td>
  //         </tr>
  //       );
  //     }
  //   });
  // };


 const checkIn = (status, data, index) => {
   if (status === 'parent'){
      if (data === null) {
        if (index > 0) {
          return `${not} DATANG`
        } else {
          return `${notYet} DATANG HARI INI`
        }
      }
   } else {
      if (data === null) {
          return `${not} DATANG`
      } else {
          return data.slice(11);
      }
   }

 }

 const checkOut = (status, data, index) => {
  if (status === 'parent'){
    if (data === null) {
      if (index > 0) {
        return `${not} PULANG`
      } else {
        return `${notYet} PULANG HARI INI`
      }
    }
 } else {
    if (data === null) {
        return `${not} PULANG`
    } else {
        return data.slice(11);
    }
 }
 }

 const checkDate = (status, data) => {
   if (status === 'parent') {
      if (data !== null) {
          if (data.length === 10) {
              return data;
          } else {
              const raw = data;
              const date = [
                raw.slice(6),
                raw.slice(4, 6),
                raw.slice(0, 4)
              ].join("-");
              return date;
          }
      } else {
        const yesterday = new Date(Date.now() - 86400000);
        return (
          String(yesterday.getDate()).padStart(2, "0") +
          "-" +
          String(yesterday.getMonth() + 1).padStart(2, "0") +
          "-" +
          yesterday.getFullYear()
        );
      }
   } else {
      if (data === null) {
        const yesterday = new Date(Date.now() - 172800000);
        return (
          String(yesterday.getDate()).padStart(2, "0") +
          "-" +
          String(yesterday.getMonth() + 1).padStart(2, "0") +
          "-" +
          yesterday.getFullYear()
        );
      } else {
          if (data.length === 10) {
              return data;
          } else {
            const raw = data;
            const date = [
              raw.slice(6),
              raw.slice(4, 6),
              raw.slice(0, 4)
            ].join("-");
            return date;
          }
      }

   }

 }

  const showPresenceTwo = () => {
    return presence.map((data,index)=> {

      if (data.length > 1)
      {
        return data.map((data, index) => {
          return (
            <tr key={index}>
              <td className="pl-0">
                <div className="d-flex align-items-center">
                  <div className="symbol symbol-40 symbol-light mr-4">
                    <span className="symbol-label">
                      <i
                        className="flaticon-calendar-with-a-clock-time-tools text-warning"
                        style={{ fontSize: "1.7rem" }}
                      ></i>
                    </span>
                  </div>
                  <div>
                    <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
                      {checkDate('child', data.TANGGAL_ABSEN)}
                    </span>
                  </div>
                </div>
              </td>
              <td className="text-center">
                <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
                  {checkIn('child', data.MASUK, index)}
                </span>
              </td>
    
              <td className="text-center">
                <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
                  {checkOut('child', data.KELUAR, index)}
                </span>
              </td>
            </tr>
          );
        })
        
      }
      else {
        return (
          <tr key={index}>
            <td className="pl-0">
              <div className="d-flex align-items-center">
                <div className="symbol symbol-40 symbol-light mr-4">
                  <span className="symbol-label">
                    <i
                      className="flaticon-calendar-with-a-clock-time-tools text-warning"
                      style={{ fontSize: "1.7rem" }}
                    ></i>
                  </span>
                </div>
                <div>
                  <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
                    {checkDate('parent', data.TANGGAL_ABSEN)}
                  </span>
                </div>
              </div>
            </td>
            <td className="text-center">
              <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
                {checkIn('parent', data.MASUK, index)}
              </span>
            </td>
  
            <td className="text-center">
              <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
                {checkOut('parent', data.KELUAR, index)}
              </span>
            </td>
          </tr>
        );
      }
      
    })
  }

  return (
    <>
      <div className="tab-content">
        <div className="table-responsive">
          <table className="table table-head-custom table-head-bg table-borderless table-vertical-center">
            <thead>
              <tr className="text-left text-uppercase">
                <th className="pl-20" style={{ minWidth: "150px" }}>
                  Tanggal
                </th>
                <th style={{ minWidth: "100px", textAlign: "center" }}>
                  Presensi Masuk
                </th>
                <th style={{ minWidth: "100px", textAlign: "center" }}>
                  Presensi Keluar
                </th>
              </tr>
            </thead>
            <tbody>{showPresenceTwo()}</tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default Presence;
