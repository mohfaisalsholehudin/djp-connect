import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next'
import {
  Card,
  CardBody
} from '../../../src/_metronic/_partials/controls';
import {Card as card} from 'react-bootstrap';

function MyTasks () {

const products = [{
    id: '1',
    name: 'John',
    price: '10000'
}]
  const columns = [
    {
      dataField: 'id',
      text: 'Product ID'
    },
    {
      dataField: 'name',
      text: 'Product Name'
    },
    {
      dataField: 'price',
      text: 'Product Price'
    }
  ]
  return (
    <>
      <Card style={{
              borderLeft: "10px solid #FFC91B",
              borderRight: "10px solid #FFC91B",
              borderBottom: "10px solid #FFC91B",
              borderTop: "1px solid #FFC91B"
            }}>
        <card.Header style={{
                backgroundColor: "#FFC91B",
                fontWeight: "700",
                borderRadius: "0px"
              }}>
            <div className="card-title">
                <div className="card-label" >
                    Meja Kerjaku
                </div>

            </div>
        </card.Header>
        <CardBody>
          <BootstrapTable
            keyField='id'
            data={products}
            columns={columns}
            bordered={false}
            search
          />
        </CardBody>
      </Card>
    </>
  )
}

export default MyTasks
