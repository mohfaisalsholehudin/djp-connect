import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../_metronic/_partials/controls";
import CustomImageInput from "./CustomImageInput"

// Validation schema
const AdsSchema = Yup.object().shape({
  title: Yup.string()
    .min(3, "Minimum 3 Characters")
    .max(50, "Maximum 50 Characters")
    .required("Title is required"),
  link: Yup.string()
    .matches(
      /((https?):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
      "Enter correct url!"
    )
    .required("Link is required"),
    file: Yup
        .mixed()
        .required("A file is required")
        .test(
          "fileSize",
          "File too large",
          value => value && value.size <= FILE_SIZE
        )
        .test(
          "fileFormat",
          "Unsupported Format",
          value => value && SUPPORTED_FORMATS.includes(value.type)
        ) 
});

const FILE_SIZE = 160 * 1024;
    const SUPPORTED_FORMATS = [
      "image/jpg",
      "image/jpeg",
      "image/gif",
      "image/png"
    ];
function AdsForm({ id, onHide }) {
  const [initialValues, setInitialValues] = useState({
    title: "",
    link: "",
    file: "",
  });

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={AdsSchema}
        onSubmit={(values) => {
          console.log(values);
          // saveApps(values);
        }}
      >
        {({handleSubmit, setFieldValue, handleBlur, errors, touched, values}) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              <Form className="form form-label-right">
                <div className="form-group row">
                  {/* Title */}
                  <div className="col-lg-4">
                    <Field
                      name="title"
                      component={Input}
                      placeholder="Enter Ads Title"
                      label="Title"
                    />
                  </div>
                  {/* Link */}
                  <div className="col-lg-4">
                    <Field
                      name="link"
                      component={Input}
                      placeholder="Enter Ads Title"
                      label="Link"
                    />
                  </div>
                  {/* Image */}
                  
                </div>
                <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                name="file"
                component={CustomImageInput}
                title="Select a file"
                setFieldValue={setFieldValue}
                errorMessage={errors["file"] ? errors["file"] : undefined}
                touched={touched["file"]}
                style={{ display: "flex" }}
                onBlur={handleBlur}
              />
                

                    {/* <input
                      type="file"
                      name="file"
                      onChange={(event) =>{
                        setFieldValue("file", event.target.files[0]);
                      }}
                        
                    /> */}
                    {/* <Field
                      name="file"
                      type="file"
                      component={Input}
                      label="File"
                    /> */}
                  </div>
                </div>
                <pre>
                {values.file
                  ? JSON.stringify(
                      {
                        fileName: values.file.name,
                        type: values.file.type,
                        size: `${values.file.size} bytes`
                      },
                      null,
                      2
                    )
                  : null}
              </pre>
              <pre>{values.text ? values.text : null} </pre>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}

export default AdsForm;
