import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import axios from "axios";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
import AdsForm from "./AdsForm";

export function AdsEdit({ id, show, onHide }) {
  const [img, setImg] = useState([]);
  const getAds = (id) => {
    
    if (id !== null) {
        axios.get(`/api/ads/${id}`).then(({ data }) => {
            setImg(data);
            console.log(data);
          });
    }
    
  };

  useEffect(() => {
    // getAds(id);
  }, [id]);

  const checkTitle = (id) => {

        let _title = id ? "Edit Ads" : "New Ads";

    return (
        <>
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">{_title}</Modal.Title>
            </Modal.Header>
        </>
    );
  }

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {checkTitle(id)}
          <AdsForm id={id} onHide={onHide} />
    </Modal>
  );
}
