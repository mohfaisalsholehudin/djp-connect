import BootstrapTable from "react-bootstrap-table-next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { Modal, Button, Card as card } from "react-bootstrap";
import {
  Card,
  CardBody,
} from "../../../../src/_metronic/_partials/controls";
import {
  sortCaret,
  headerSortingClasses,
  toAbsoluteUrl,
} from "../../../../src/_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { useHistory } from "react-router-dom";
import { withRouter, Route } from "react-router-dom";
import { AdsOpen } from "./AdsOpen";
import { AdsEdit } from "./AdsEdit";

function Ads() {
  const history = useHistory();
  const [adu, setAdu] = useState([]);
  const [isMore, setIsMore] = useState(false);

  const getAds = async () => {
    try {
      await axios.get("/api/ads?page=1").then(({ data }) => {
        data["hydra:totalItems"] < 3 && setIsMore(true);

        data["hydra:member"].map((data) => {
          const links = {
            linkImage: toAbsoluteUrl(`/media/ads/${data.linkImage}`),
            link: data.link,
            title: data.title,
            id: data.id,
            createdAt: data.createdAt,
          };
          const adu = [];
          adu.push(links);
          return setAdu(adu);
        });
      });
    } catch (e) {
      console.log(e);
    }
  };
  const openAds = (id) => history.push(`/ads/${id}/open`);
  const addAds = () => history.push('/ads/add');
  const editAds = (id) => history.push(`/ads/${id}/edit`);

  // const addAction = () => props.history.push("/apps/add");
  // const editAction = (id) => props.history.push(`/apps/${id}/edit`);
  // const deleteAction = (id) =>  props.history.push(`/apps/${id}/delete`);

  const imageFormat = (cell) => {
    return (
      <>
        <img src={cell} style={{ width: "350px" }} />
      </>
    );
  };

  const columns = [
    {
      dataField: "title",
      text: "Title",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "link",
      text: "Link",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "linkImage",
      text: "Ads",
      formatter: imageFormat,
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditDialog: editAds,
        // openDeleteDialog: deleteAction,
        showAds: openAds,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  useEffect(() => {
    getAds();
  }, []);

  return (
    <>
      <Card
      style={{
        borderLeft: "10px solid #1D428A",
        borderRight: "10px solid #1D428A",
        borderBottom: "10px solid #1D428A",
        borderTop: "1px solid #1D428A",
      }}>
        <card.Header
        style={{
          backgroundColor: "#1D428A",
          fontWeight: "700",
          borderRadius: "0px",
          padding: "2rem 2.25rem"
        }}>
          <div className="card-title">
            <h3 className="card-label" style={{color: "#fff"}}>
              ADS
            </h3>
          </div>
          <div className="card-toolbar">
          {isMore && (
              <button
                type="button"
                className="btn btn-warning"
                style={{color: "#000", fontWeight: "500"}}
                onClick={() => addAds()}
              >
                Add Ads
              </button>
            )}
          </div>
         
        </card.Header>

        <CardBody>
          <>
            <BootstrapTable
              wrapperClasses="table-responsive"
              bordered={false}
              classes="table table-head-custom table-vertical-center overflow-hidden"
              bootstrap4
              keyField="id"
              data={adu}
              columns={columns}
            ></BootstrapTable>
          </>
        </CardBody>
      </Card>

      <Route path="/ads/:id/open">
        {({ history, match }) => (
          <AdsOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/ads");
            }}
            onRef={() => {
              history.push("/ads");
            }}
          />
        )}
      </Route>

      {/*Route to Add Apps*/}
      <Route path="/ads/add">
            {({ history, match }) => (
                <AdsEdit
                    show={match != null}
                    onHide={() => {
                       history.push("/ads");
                    }}

                    onRef={() => {
                        history.push("/ads");
                    }}
                />
            )}
        </Route>

        {/*Route to Edit Apps*/}
      <Route path="/ads/:id/edit">
            {({ history, match }) => (
                <AdsEdit
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/ads");
                    }}
                    onRef={() => {
                        history.push("/ads");
                    }}
                />
            )}
        </Route>

      {/*Route to Delete Apps*/}
      {/* <Route path="/apps/:id/delete">
            {({ history, match }) => (
                <AppsDeleteDialog
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/apps");
                    }}
                    onRef={() => {
                        history.push("/monitoring");
                        history.replace("/apps");
                    }}
                />
            )} 
        </Route> */}
    </>
  );
}

export default Ads;
