import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import axios from "axios";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";

export function AdsOpen({ id, show, onHide }) {
  const [img, setImg] = useState([]);
  const getAds = (id) => {
    
    if (id !== null) {
        axios.get(`/api/ads/${id}`).then(({ data }) => {
            setImg(data);
            console.log(data);
          });
    }
    
  };

  useEffect(() => {
    getAds(id);
  }, [id]);

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">Ads</Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <img
          alt={img.title}
          src={toAbsoluteUrl(`/media/ads/${img.linkImage}`)}
          style={{width: '100%'}}
        />
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          onClick={onHide}
          className="btn btn-light btn-elevate"
        >
          Close
        </button>
      </Modal.Footer>
    </Modal>
  );
}
