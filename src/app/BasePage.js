import React, {Suspense} from "react";
import {Redirect, Switch} from "react-router-dom";
import {LayoutSplashScreen, ContentRoute} from "../_metronic/layout";
import {BuilderPage} from "./pages/BuilderPage";
import {MyPage} from "./pages/MyPage";
import {DashboardPage} from "./pages/DashboardPage";
import MyTasks from "./pages/MyTasks";
import Notification from "./pages/Notification";
import Announcement from "./pages/Announcement";
// import Ads from "./pages/ads/Ads";


export default function BasePage() {
    // useEffect(() => {
    //   console.log('Base page');
    // }, []) // [] - is required if you need only one call
    // https://reactjs.org/docs/hooks-reference.html#useeffect

    return (
        <Suspense fallback={<LayoutSplashScreen/>}>
            <Switch>
                {
                    /* Redirect from root URL to /dashboard. */
                    <Redirect exact from="/" to="/beranda"/>
                }
                <ContentRoute path="/beranda" component={DashboardPage}/>
                <ContentRoute path="/mytask" component={MyTasks}/>
                <ContentRoute path="/notification" component={Notification}/>
                <ContentRoute path="/announcement" component={Announcement}/>
                <ContentRoute path="/builder" component={BuilderPage}/>
                <ContentRoute path="/my-page" component={MyPage}/>
                {/* <ContentRoute path="/ads" component={Ads}/> */}
                <Redirect to="error/error-v1"/>
            </Switch>
        </Suspense>
    );
}
