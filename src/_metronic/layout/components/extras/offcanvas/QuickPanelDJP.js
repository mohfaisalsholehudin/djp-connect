/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,no-undef */
import React, { useState, useEffect } from "react";
import { Tab, Nav } from "react-bootstrap";
import { toAbsoluteUrl } from "../../../../_helpers";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from "axios";

export function QuickPanelDJP() {
  const [selectedTab, setSelectedTab] = useState("Main Application");
  const [commonApp, setCommonApp] = useState([]);
  const [roleApp, setRoleApp] = useState([]);

  const {  iamToken } = useSelector((state) => state.auth);
  const { MIDDLEWARE } = window.ENV;

  const setTab = (_tabName) => {
    setSelectedTab(_tabName);
  };

  const getCommonApp = async () => {
    try {
      const config = {
        "Content-type": "application/json",
        Authorization: `Bearer ${iamToken}`,
      };
      const response = await axios.post(
        `${MIDDLEWARE}/api/getaplikasibytoken`,
        // `/api/getaplikasibytoken`,
        null,
        {
          headers: config,
        }
      );
      return response;
    } catch (err) {
      console.log(err)
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  };

 

  useEffect(() => {
    getCommonApp().then(({ data: { aplikasi } }) => {
      if (aplikasi === undefined){
        setCommonApp([]);
      } else {
        setCommonApp(aplikasi)
      }
    });
  }, []);

  const showCommonApp = () => {
    return commonApp.map((data, index) => {
      const color = (index) => {
        if (index % 2 === 0) {
          return "#1D428A";
        } else {
          return "#FFC91B";
        }
      };

      const font = (index) => {
        if (index % 2 === 0) {
          return "#fff";
        } else {
          return "#1D428A";
        }
      };

      const logo = (index) => {
        if (index % 2 === 0) {
          return "icon-app-white.png";
        } else {
          return "icon-app-blue.png";
        }
      };

      return (
        <div
          className="col-lg-12 align-items-center rounded p-5 gutter-b"
          style={{ backgroundColor: color(index) }}
          key={index}
        >
          <div className="row">
            <span className="col-lg-3">
              <img
                width="100%"
                src={toAbsoluteUrl(`/media/logos/${logo(index)}`)}
                alt="images"
              />
            </span>
            <div
              className="col-lg-9"
              style={{
                display: "flex",
                alignContent: "center",
                alignItems: "center",
                textAlign: "center",
              }}
            >
              <a
                href={data.url}
                target="_blank"
                rel="noopener noreferrer"
                className={`text-white text-hover-primary font-size-lg mb-1`}
                style={{ fontSize: "x-large", fontWeight: "600" }}
                ref={(el) => {
                  if (el) {
                    el.style.setProperty(
                      "color",
                      `${font(index)}`,
                      "important"
                    );
                  }
                }}
              >
                {data.nama}
              </a>
            </div>
          </div>
        </div>
      );
    });
  };

  const showRoleApp = () => {
    return roleApp.map((data, index) => {
      return data.map((data, index) => {
        const color = (index) => {
          if (index % 2 === 0) {
            return "#1D428A";
          } else {
            return "#FFC91B";
          }
        };

        const font = (index) => {
          if (index % 2 === 0) {
            return "#fff";
          } else {
            return "#1D428A";
          }
        };

        const logo = (index) => {
          if (index % 2 === 0) {
            return "icon-app-white.png";
          } else {
            return "icon-app-blue.png";
          }
        };

        return (
          <div
            className="col-lg-12 align-items-center rounded p-5 gutter-b"
            style={{ backgroundColor: color(index) }}
            key={data.id}
          >
            <div className="row">
              <span className="col-lg-3">
                <img
                  width="100%"
                  src={toAbsoluteUrl(`/media/logos/${logo(index)}`)}
                  alt="images"
                />
              </span>
              <div
                className="col-lg-9"
                style={{
                  display: "flex",
                  alignContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                }}
              >
                <a
                  href={data.url}
                  target="_blank"
                  rel="noopener noreferrer"
                  className={`text-white text-hover-primary font-size-lg mb-1`}
                  style={{ fontSize: "x-large", fontWeight: "600" }}
                  ref={(el) => {
                    if (el) {
                      el.style.setProperty(
                        "color",
                        `${font(index)}`,
                        "important"
                      );
                    }
                  }}
                >
                  {data.nama}
                </a>
              </div>
            </div>
          </div>
        );
      });
      // <div
      //   className="col-lg-12 align-items-center rounded p-5 gutter-b"
      //   style={{ backgroundColor: "#1D428A" }}
      //   key={index}
      // >
      //   <div className="row">
      //     <span className="col-lg-3">
      //       <img
      //         width="100%"
      //         src={toAbsoluteUrl("/media/logos/djp-logo.png")}
      //         alt="images"
      //       />
      //     </span>
      //     <div
      //       className="col-lg-9"
      //       style={{
      //         display: "flex",
      //         alignContent: "center",
      //         alignItems: "center",
      //         textAlign: "center",
      //       }}
      //     >
      //       <a
      //         href={data.url}
      //         target="_blank"
      //         rel="noopener noreferrer"
      //         className="text-white text-hover-primary font-size-lg mb-1"
      //         style={{ fontSize: "x-large", fontWeight: "600" }}
      //       >
      //         {data.nama}
      //       </a>
      //     </div>
      //   </div>
      // </div>
    });
  };

  return (
    <div id="kt_quick_panel" className="offcanvas offcanvas-left pt-5 pb-10">
      <Tab.Container defaultActiveKey={selectedTab}>
        {/*begin::Header*/}
        <div className="offcanvas-header offcanvas-header-navs d-flex align-items-center justify-content-between mb-5">
          <Nav
            onSelect={setTab}
            as="ul"
            role="tablist"
            className="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-primary flex-grow-1 px-10"
          >
            <Nav.Item as="li">
              <Nav.Link
                eventKey="Main Application"
                className={`nav-link ${
                  selectedTab === "Main Application" ? "active" : ""
                }`}
              >
                Applications
              </Nav.Link>
            </Nav.Item>
            {/* <Nav.Item className="nav-item" as="li">
              <Nav.Link
                eventKey="app role"
                className={`nav-link ${
                  selectedTab === "app role" ? "active" : ""
                }`}
              >
                Application by Role
              </Nav.Link>
            </Nav.Item> */}
            {/* <Nav.Item className="nav-item" as="li">
              <Nav.Link
                eventKey="app test"
                className={`nav-link ${
                  selectedTab === "app test" ? "active" : ""
                }`}
              >
                Main Application
              </Nav.Link>
            </Nav.Item> */}
          </Nav>

          <div
            className="offcanvas-close mt-n1 pr-5"
            style={{ position: "absolute", top: "15px", right: "10px" }}
          >
            <a
              href="#"
              className="btn btn-xs btn-icon btn-light btn-hover-primary"
              id="kt_quick_panel_close"
            >
              <i className="ki ki-close icon-xs text-muted"></i>
            </a>
          </div>
        </div>
        {/*end::Header*/}

        {/*begin::Content*/}
        <div className="offcanvas-content px-10">
          <div className="tab-content">
            {/* Begin::Common Application */}
            <div
              id="kt_quick_panel_logs"
              role="tabpanel"
              className={`tab-pane fade pt-3 pr-5 mr-n5 scroll ps ${
                selectedTab === "Main Application" ? "active show" : ""
              }`}
            >
              <div className="mb-5">
                {/* <h5 className="font-weight-bold mb-5">Aplikasi Core</h5> */}

                {/* Begin::First Application */}
                {/* <div
                  className="d-flex align-items-center rounded p-5 gutter-b"
                  style={{ backgroundColor: "#1D428A" }}
                >
                  <span className="svg-icon svg-icon mr-5">
                    <a
                      href="https://sikka-djp/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <img
                        height="40px"
                        width="200px"
                        src={toAbsoluteUrl("/media/logos/logo-sikka.png")}
                        alt="images"
                      />
                    </a>
                  </span>
                </div> */}
                {/* End::First Application */}

                {/* Begin::Temporary Application */}
                {showCommonApp()}
                {showRoleApp()}
                {/* End::Temporary Application */}

                {/* Begin::FIX Application */}
                {/* <a
                      href="https://sikka-djp/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                  <Card className="bg-dark text-white">
                    <Card.Img src={toAbsoluteUrl("/media/logos/logo-sikka.png")} alt="Card image" />
                  </Card>
                  </a> */}
                {/* End::FIX Application */}
              </div>
              {/* <div className="separator separator-dashed mt-8 mb-5" />

              <div className="mb-5">
                <h5 className="font-weight-bold mb-5">Aplikasi TPT</h5>

                <div
                  className="d-flex align-items-center rounded p-5 gutter-b"
                  style={{ backgroundColor: "#1D428A" }}
                >
                  <span className="svg-icon svg-icon mr-5">
                    <a href="https://sikka-djp/" target="_blank">
                      <img
                        height="40px"
                        width="200px"
                        src={toAbsoluteUrl("/media/logos/logo-sikka.png")}
                        alt="images"
                      />
                    </a>
                  </span>
                </div>
                <div
                  className="col-lg-12 align-items-center rounded p-5 gutter-b"
                  style={{ backgroundColor: "#1D428A" }}
                >
                  <div className="row">
                    <span className="col-lg-3">
                      <img
                        width="100%"
                        src={toAbsoluteUrl("/media/logos/djp-logo.png")}
                        alt="images"
                      />
                    </span>
                    <div
                      className="col-lg-9"
                      style={{
                        display: "flex",
                        alignContent: "center",
                        alignItems: "center",
                        textAlign: "center"
                      }}
                    >
                      <a
                        href="https://logbook.pajak.go.id"
                        target="_blank"
                        className="text-white text-hover-primary font-size-lg mb-1"
                        style={{ fontSize: "x-large", fontWeight: "600" }}
                      >
                        Logbook
                      </a>
                    </div>
                  </div>
                </div>
              </div> */}
              <div className="separator separator-dashed mt-8 mb-5" />
            </div>
            {/* End::Common Application */}

            {/* Begin::Application By Role */}
            <div
              id="kt_quick_panel_logs"
              role="tabpanel"
              className={`tab-pane fade pt-3 pr-5 mr-n5 scroll ps ${
                selectedTab === "app role" ? "active show" : ""
              }`}
            >
              <div className="mb-5">
                {/* <h5 className="font-weight-bold mb-5">Aplikasi</h5> */}
                {showRoleApp()}

                {/* <div className="d-flex align-items-center bg-light-warning rounded p-5 mb-5">
                  <span className="svg-icon svg-icon-warning mr-5">
                    <SVG
                      src={toAbsoluteUrl("/media//svg/icons/Home/Library.svg")}
                      className="svg-icon svg-icon-lg"
                    ></SVG>
                  </span>

                  <div className="d-flex flex-column flex-grow-1 mr-2">
                    <a
                      href="#"
                      className="font-weight-normal text-dark-75 text-hover-primary font-size-lg mb-1"
                    >
                      Another purpose persuade
                    </a>
                    <span className="text-muted font-size-sm">
                      Due in 2 Days
                    </span>
                  </div>

                  <span className="font-weight-bolder text-warning py-1 font-size-lg">
                    +28%
                  </span>
                </div>

                <div className="d-flex align-items-center bg-light-success rounded p-5 mb-5">
                  <span className="svg-icon svg-icon-success mr-5">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/Communication/Write.svg"
                      )}
                      className="svg-icon svg-icon-lg"
                    ></SVG>
                  </span>
                  <div className="d-flex flex-column flex-grow-1 mr-2">
                    <a
                      href="#"
                      className="font-weight-normal text-dark-75 text-hover-primary font-size-lg mb-1"
                    >
                      Would be to people
                    </a>
                    <span className="text-muted font-size-sm">
                      Due in 2 Days
                    </span>
                  </div>

                  <span className="font-weight-bolder text-success py-1 font-size-lg">
                    +50%
                  </span>
                </div>

                <div className="d-flex align-items-center bg-light-danger rounded p-5 mb-5">
                  <span className="svg-icon svg-icon-danger mr-5">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/Communication/Group-chat.svg"
                      )}
                      className="svg-icon svg-icon-lg"
                    ></SVG>
                  </span>
                  <div className="d-flex flex-column flex-grow-1 mr-2">
                    <a
                      href="#"
                      className="font-weight-normel text-dark-75 text-hover-primary font-size-lg mb-1"
                    >
                      Purpose would be to persuade
                    </a>
                    <span className="text-muted font-size-sm">
                      Due in 2 Days
                    </span>
                  </div>

                  <span className="font-weight-bolder text-danger py-1 font-size-lg">
                    -27%
                  </span>
                </div>

                <div className="d-flex align-items-center bg-light-info rounded p-5">
                  <span className="svg-icon svg-icon-info mr-5">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/General/Attachment2.svg"
                      )}
                      className="svg-icon svg-icon-lg"
                    ></SVG>
                  </span>

                  <div className="d-flex flex-column flex-grow-1 mr-2">
                    <a
                      href="#"
                      className="font-weight-normel text-dark-75 text-hover-primary font-size-lg mb-1"
                    >
                      The best product
                    </a>
                    <span className="text-muted font-size-sm">
                      Due in 2 Days
                    </span>
                  </div>

                  <span className="font-weight-bolder text-info py-1 font-size-lg">
                    +8%
                  </span>
                </div> */}
              </div>
            </div>
            {/* End::Application By Role */}

            {/* Begin::Taxpayer Application */}
            <div
              id="kt_quick_panel_logs"
              role="tabpanel"
              className={`tab-pane fade pt-3 pr-5 mr-n5 scroll ps ${
                selectedTab === "app test" ? "active show" : ""
              }`}
            >
              <div
                id="kt_aside_menu"
                data-menu-vertical="1"
                className={`aside-menu my-4`}
                data-menu-scroll="1"
                data-menu-dropdown-timeout="500"
                style={{ overflow: "auto", height: "1345px" }}
              >
                <ul className={`menu-nav`}>
                  <li
                    className={`menu-item menu-item-submenu 
              `}
                    aria-haspopup="true"
                    data-menu-toggle="hover"
                  >
                    <NavLink
                      className="menu-link menu-toggle"
                      to="/core"
                      style={{ marginBottom: "20px" }}
                    >
                      {/* <span className="menu-text">Aplikasi Core</span> */}
                      <h5
                        className="font-weight-bold mb-5"
                        style={{ marginTop: "14px" }}
                      >
                        Aplikasi Core
                      </h5>

                      <i
                        className="menu-arrow"
                        style={{
                          marginTop: "auto",
                          marginBottom: "auto",
                          marginLeft: "120px",
                          marginRight: "auto",
                        }}
                      />
                    </NavLink>
                    <div className="menu-submenu ">
                      <i className="menu-arrow" />
                      <ul className="menu-subnav">
                        <li
                          className="menu-item  menu-item-parent"
                          aria-haspopup="true"
                        >
                          {/* <span className="menu-link">
                            <span className="menu-text">Aplikasi Core</span>
                          </span> */}
                        </li>

                        {/*begin::2 Level*/}
                        <li className={`menu-item `} aria-haspopup="true">
                          <div
                            className="d-flex align-items-center rounded p-5 gutter-b"
                            style={{ backgroundColor: "#1D428A" }}
                          >
                            <span className="svg-icon svg-icon mr-5">
                              <a
                                href="https://sikka-djp/"
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <img
                                  height="40px"
                                  width="200px"
                                  src={toAbsoluteUrl(
                                    "/media/logos/logo-sikka.png"
                                  )}
                                  alt="images"
                                />
                              </a>
                            </span>
                          </div>
                        </li>
                        <li className={`menu-item `} aria-haspopup="true">
                          <div
                            className="d-flex align-items-center rounded p-5 gutter-b"
                            style={{ backgroundColor: "#1D428A" }}
                          >
                            <span className="svg-icon svg-icon mr-5">
                              <a
                                href="https://sikka-djp/"
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <img
                                  height="40px"
                                  width="200px"
                                  src={toAbsoluteUrl(
                                    "/media/logos/logo-sikka.png"
                                  )}
                                  alt="images"
                                />
                              </a>
                            </span>
                          </div>
                        </li>
                        {/*end::2 Level*/}
                      </ul>
                    </div>
                  </li>
                  <li
                    className={`menu-item menu-item-submenu 
              `}
                    aria-haspopup="true"
                    data-menu-toggle="hover"
                  >
                    <NavLink
                      className="menu-link menu-toggle"
                      to="/core"
                      style={{ marginBottom: "20px" }}
                    >
                      {/* <span className="menu-text">Aplikasi Core</span> */}
                      <h5
                        className="font-weight-bold mb-5"
                        style={{ marginTop: "14px" }}
                      >
                        Aplikasi TPT
                      </h5>

                      <i
                        className="menu-arrow"
                        style={{
                          marginTop: "auto",
                          marginBottom: "auto",
                          marginLeft: "120px",
                          marginRight: "auto",
                        }}
                      />
                    </NavLink>
                    <div className="menu-submenu ">
                      <i className="menu-arrow" />
                      <ul className="menu-subnav">
                        <li
                          className="menu-item  menu-item-parent"
                          aria-haspopup="true"
                        >
                          {/* <span className="menu-link">
                            <span className="menu-text">Aplikasi Core</span>
                          </span> */}
                        </li>

                        {/*begin::2 Level*/}
                        <li className={`menu-item `} aria-haspopup="true">
                          <div
                            className="d-flex align-items-center rounded p-5 gutter-b"
                            style={{ backgroundColor: "#1D428A" }}
                          >
                            <span className="svg-icon svg-icon mr-5">
                              <a
                                href="https://sikka-djp/"
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <img
                                  height="40px"
                                  width="200px"
                                  src={toAbsoluteUrl(
                                    "/media/logos/logo-sikka.png"
                                  )}
                                  alt="images"
                                />
                              </a>
                            </span>
                          </div>
                        </li>
                        <li className={`menu-item `} aria-haspopup="true">
                          <div
                            className="d-flex align-items-center rounded p-5 gutter-b"
                            style={{ backgroundColor: "#1D428A" }}
                          >
                            <span className="svg-icon svg-icon mr-5">
                              <a
                                href="https://sikka-djp/"
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <img
                                  height="40px"
                                  width="200px"
                                  src={toAbsoluteUrl(
                                    "/media/logos/logo-sikka.png"
                                  )}
                                  alt="images"
                                />
                              </a>
                            </span>
                          </div>
                        </li>
                        {/*end::2 Level*/}
                      </ul>
                    </div>
                  </li>
                  {/*end::1 Level*/}
                </ul>
              </div>
            </div>
            {/* End::Taxpayer Application */}
          </div>
        </div>
        {/*end::Content*/}
      </Tab.Container>
    </div>
  );
}
