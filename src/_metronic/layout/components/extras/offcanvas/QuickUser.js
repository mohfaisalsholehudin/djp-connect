/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,no-undef */
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { toAbsoluteUrl } from "../../../../_helpers";
import { useSelector } from "react-redux";

export function QuickUser() {
  const history = useHistory();

  const { user } = useSelector((state) => state.auth);
  const [pegawai] = useState([]);
  const unit = user.pegawai.jabatan_pegawais.length > 0 ? user.pegawai.jabatan_pegawais[0].nama_unit : "";
  const jabatan = user.pegawai.jabatan_pegawais.length > 0 ? user.pegawai.jabatan_pegawais[0].nama_jabatan : "";

  const showNamaJabatan = () => {
    return pegawai.map((data, index) => (
      <div key={index}>
        <div key={data.jabatan["@id"]} className="text-muted mt-1">
          {data.jabatan.nama}
        </div>
        <div key={data.unit["@id"]} className="text-muted mt-1">
          {data.unit.nama}
        </div>
      </div>
    ));
  };

  const logoutClick = () => {
    const toggle = document.getElementById("kt_quick_user_toggle");
    if (toggle) {
      toggle.click();
    }

    // window.open("http://localhost:8081/logout");
    // setTimeout(() => {
    //   history.push("/logout");
    // }, 1000);

    history.push("/logout");
  };


  return (
    <div
      id="kt_quick_user"
      className="offcanvas offcanvas-right offcanvas p-10"
    >
      <div className="offcanvas-header d-flex align-items-center justify-content-between pb-5">
        <h3 className="font-weight-bold m-0">
          User Profile
          {/* <small className="text-muted font-size-sm ml-2">12 messages</small> */}
        </h3>
        <a
          href="#"
          className="btn btn-xs btn-icon btn-light btn-hover-primary"
          id="kt_quick_user_close"
        >
          <i className="ki ki-close icon-xs text-muted" />
        </a>
      </div>

      <div className="offcanvas-content pr-5 mr-n5">
        <div className="d-flex align-items-center mt-5">
          <div className="symbol symbol-100 mr-5">
            <div
              className="symbol-label"
              style={{
                backgroundImage: `url(${toAbsoluteUrl(
                  "/media/users/300_21.jpg"
                )})`,
              }}
            />
            <i className="symbol-badge bg-success" />
          </div>
          <div className="d-flex flex-column">
            <a
              href="#"
              className="font-weight-bold font-size-h5 text-dark-75 text-hover-primary"
            >
              {user.pegawai.nama}
            </a>
            {/* <div className="text-muted mt-1">Application Developer</div> */}
            <div className="text-muted mt-1">{user.pegawai.nip18}</div>
            {showNamaJabatan()}
            <div>
              <div className="text-muted mt-1">{jabatan}</div>
              <div className="text-muted mt-1">{unit}</div>
            </div>
            <br />

            {/* <div className="navi mt-2">
                <a href="#" className="navi-item">
                <span className="navi-link p-0 pb-2">
                  <span className="navi-icon mr-1">
                    <span className="svg-icon-lg svg-icon-primary">
                      <SVG
                          src={toAbsoluteUrl(
                              "/media/svg/icons/Communication/Mail-notification.svg"
                          )}
                      ></SVG>
                    </span>
                  </span>
                  <span className="navi-text text-muted text-hover-primary">
                    jm@softplus.com
                  </span>
                </span>
                </a>
              </div> */}
            {/* <Link to="/logout" className="btn btn-light-primary btn-bold">
                Sign Out
              </Link> */}
            <button
              className="btn  btn-bold"
              onClick={logoutClick}
              style={{ backgroundColor: "#FFC91B" }}
            >
              Keluar
            </button>
          </div>
        </div>

        <div className="separator separator-dashed mt-8 mb-5" />

        <div className="navi navi-spacer-x-0 p-0">
          {/* <a href="/user/profile" className="navi-item">
            <div className="navi-link">
              <div className="symbol symbol-40 bg-light mr-3">
                <div className="symbol-label">
                  <span className="svg-icon svg-icon-md svg-icon-success">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/General/Notification2.svg"
                      )}
                    ></SVG>
                  </span>
                </div>
              </div>
              <div className="navi-text">
                <div className="font-weight-bold">My Profile</div>
                <div className="text-muted">
                  Account settings and more{" "}
                  <span className="label label-light-danger label-inline font-weight-bold">
                    update
                  </span>
                </div>
              </div>
            </div>
          </a> */}

          {/* <a href="/user/profile" className="navi-item">
            <div className="navi-link">
              <div className="symbol symbol-40 bg-light mr-3">
                <div className="symbol-label">
                  <span className="svg-icon svg-icon-md svg-icon-warning">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/Shopping/Chart-bar1.svg"
                      )}
                    ></SVG>
                  </span>
                </div>
              </div>
              <div className="navi-text">
                <div className="font-weight-bold">My Messages</div>
                <div className="text-muted">Inbox and tasks</div>
              </div>
            </div>
          </a> */}

          {/* <a href="/user/profile" className="navi-item">
            <div className="navi-link">
              <div className="symbol symbol-40 bg-light mr-3">
                <div className="symbol-label">
                  <span className="svg-icon svg-icon-md svg-icon-danger">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/Files/Selected-file.svg"
                      )}
                    ></SVG>
                  </span>
                </div>
              </div>
              <div className="navi-text">
                <div className="font-weight-bold">My Activities</div>
                <div className="text-muted">Logs and notifications</div>
              </div>
            </div>
          </a> */}

          {/* <a href="/user/profile" className="navi-item">
            <div className="navi-link">
              <div className="symbol symbol-40 bg-light mr-3">
                <div className="symbol-label">
                  <span className="svg-icon svg-icon-md svg-icon-primary">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/Communication/Mail-opened.svg"
                      )}
                    ></SVG>
                  </span>
                </div>
              </div>
              <div className="navi-text">
                <div className="font-weight-bold">My Tasks</div>
                <div className="text-muted">latest tasks and projects</div>
              </div>
            </div>
          </a> */}
        </div>

        {/* <div className="separator separator-dashed my-7"></div> */}

        {/* <div>
          <h5 className="mb-5">List Aplikasi</h5>

          <div
            className="d-flex align-items-center rounded p-5 gutter-b"
            style={{ backgroundColor: "#1D428A" }}
          >
            <span className="svg-icon svg-icon mr-5">
              <a href="https://sikka-djp/" target="_blank">
                <img
                  height="40px"
                  width="200px"
                  src={toAbsoluteUrl("/media/logos/logo-sikka.png")}
                  alt="images"
                />
              </a>
            </span>
          </div>
        </div> */}
        <div className="mb-10">
          <h3 className="font-weight-bold m-0 mb-5">Inbox Mail</h3>
          {/* <div className="d-flex align-items-center">
                  <div className="symbol symbol-45 bg-light mr-5">
                <div className="symbol-label">
                  <i className="flaticon2-user text-primary"></i>
                </div>
              </div>
                  <div className="d-flex flex-column flex-grow-1">
                    <a href="#" className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">
                      Bimbingan Sistem Direktorat TIK
                    </a>
                    <span className="text-muted font-weight-bold">
                      19/01/2021
                    </span>
                  </div>
                </div>
                <p className="font-weight-bold text-dark-75 font-size-md pt-5" style={{marginBottom:'-10px'}}>
                      Perpanjangan Waktu Pelaksanaan E-Bimtek
                    </p>
                <p className="text-dark-50 m-0 pt-5 font-weight-normal">
                  Aktifkan gambar untuk tampilan E-mail secara maksimal . . .
                </p> */}
        </div>
      </div>
    </div>
  );
}
