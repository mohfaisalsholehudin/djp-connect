/* eslint-disable no-restricted-imports */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useMemo, useState } from "react";
import {
  OverlayTrigger,
  Tooltip,
  Modal
} from "react-bootstrap";
import { useSelector } from "react-redux";
import objectPath from "object-path";
import { useHtmlClassService } from "../../_core/MetronicLayout";
// import { UserProfileDropdown } from "./dropdowns/UserProfileDropdown";
// import { QUser } from "./offcanvas/QUser.jsx";
import { QuickUser } from "./offcanvas/QuickUser.js";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../_helpers";


export function QuickUserToggler() {
  const { user } = useSelector(state => state.auth);
  const [modal, setModal] = useState(false);
  const uiService = useHtmlClassService();
  const layoutProps = useMemo(() => {
    return {
      offcanvas:
        objectPath.get(uiService.config, "extras.user.layout") === "offcanvas"
    };
  }, [uiService]);

  const handleKey = e => {
    if (e.key === "Enter") {
      alert("Maaf, saat ini fitur belum tersedia");
    }
  };

  return (
    <>
      {layoutProps.offcanvas && (
        <OverlayTrigger
          placement="bottom"
          overlay={<Tooltip id="quick-user-tooltip">View User</Tooltip>}
        >
          <div className="topbar-item">
            <div
              className="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2"
              id="kt_quick_user_toggle"
            >
              <>
                <span className="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">
                  Selamat Datang,
                </span>
                <span className="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">
                  {user.pegawai.nama}
                </span>
                <span className="symbol symbol-35 symbol-light-success mr-3">
                  <span
                    className="symbol-label font-size-h5 font-weight-bold"
                    style={{ backgroundColor: "#FFC91B", borderRadius: "50px" }}
                  >
                    <i className="flaticon2-user text-white"></i>
                  </span>
                </span>
                {/* <span className="symbol symbol-35 symbol-light-success">
                <span
                  className="symbol-label font-size-h5 font-weight-bold"
                  style={{ backgroundColor: "#1D428A", borderRadius: "50px" }}
                >
                  <i className="flaticon2-email text-white"></i>
                </span>
              </span> */}
              </>
            </div>
          </div>
        </OverlayTrigger>
      )}
      {/* {layoutProps.offcanvas && (
        <OverlayTrigger
          placement="bottom"
          overlay={<Tooltip id="djp-search-tooltip">DJPSearch</Tooltip>}
        >
          <div className="topbar-item">
            <div
              className="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2"
              style={{ marginLeft: "-6px" }}
              onClick={() => setModal(true)}
            >
              <>
                <span className="symbol symbol-35 symbol-light-success">
                  <span
                    className="symbol-label font-size-h5 font-weight-bold"
                    style={{ backgroundColor: "#1D428A", borderRadius: "50px" }}
                  >
                    <i className="flaticon2-search-1 text-white"></i>
                  </span>
                </span>
              </>
            </div>
          </div>
        </OverlayTrigger>
      )} */}
      {/* {!layoutProps.offcanvas && (<UserProfileDropdown/>)} */}
      {/* {!layoutProps.offcanvas && (<QUser/>)} */}
      {layoutProps.offcanvas && <QuickUser />}
    
    <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={modal}
        onHide={() => setModal(false)}
      >
        <Modal.Body>
          <div className="container">
            <div className="row" style={{ marginBottom: "50px" }}>
              <div className="col-lg-12 col-xxl-12">
                <img
                  alt="logo"
                  width="350"
                  style={{
                    marginLeft: "auto",
                    marginRight: "auto",
                    display: "block"
                  }}
                  src={toAbsoluteUrl(`/media/logos/logo-djp-search.png`)}
                />
              </div>
              <div className="col-lg-12 col-xxl-12">
                <div id="kt_quick_search_dropdown" className="quick-search">
                  <form className="quick-search-form">
                    <div
                      className="input-group"
                      style={{
                        border: "1px solid #E6E6E6",
                        borderRadius: "2rem",
                        boxShadow: "0 1px 6px 0 rgb(32 33 36 / 28%)"
                      }}
                    >
                      <div className={`input-group-prepend`}>
                        <span className="input-group-text">
                          <span className="svg-icon svg-icon-lg">
                            <SVG
                              src={toAbsoluteUrl(
                                "/media/svg/icons/General/Search.svg"
                              )}
                            />
                          </span>
                        </span>
                      </div>
                      <input
                        type="text"
                        autoFocus={true}
                        placeholder="DJPSearch"
                        // onChange={handleSearchChange}
                        onKeyDown={e => handleKey(e)}
                        className="form-control"
                      />
                    </div>
                  </form>
                </div>
                {/* <InputGroup className="mb-3">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1"
                    style={{
                      backgroundColor:'transparent',
                      border: 'none',
                      borderTop: '1px solid #e4e6ef',
                      borderBottom: '1px solid #e4e6ef',
                      borderLeft: '1px solid #e4e6ef',
                      borderBottomLeftRadius:'2rem',
                      borderTopLeftRadius:'2rem',
                      }}>
                        <i className="flaticon2-search-1 text-mute"></i>
                      </InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    placeholder="DJPSearch"
                    aria-label="DJPSearch"
                    aria-describedby="basic-addon1"
                    style={{borderLeft: '0px', borderTopRightRadius: '2rem', borderBottomRightRadius: '2rem'}}
                  />
                </InputGroup> */}
                {/* <Form>
                  <Form.Group controlId="formTextSearch">

                    <Form.Control
                      type="text"
                      placeholder="DJPSearch"
                      onKeyDown={e => handleKey(e)}
                    />
                  </Form.Group>
                </Form> */}
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>

    </>
  );
}
