/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import {OverlayTrigger, Tooltip} from "react-bootstrap";

export function StickyToolbar() {
  return (
    <>
      <div className="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4">
      <OverlayTrigger
          placement="bottom"
          overlay={<Tooltip id="layout-tooltip">Show Applications</Tooltip>}
        >
          {/* <li className="nav-item mb-2" data-placement="left">
            <Link
              to="/builder"
              className="btn btn-sm btn-icon btn-bg-light btn-text-primary btn-hover-primary"
            >
              <i className="flaticon2-gear"></i>
            </Link>
          </li> */}
          <div
            className="topbar-item"
            data-toggle="tooltip"
            title="Show Applications"
            data-placement="left"
          >
            {/* <div
              className="btn btn-icon btn-clean btn-lg mr-1"
              id="kt_quick_panel_toggle"
            > */}
              {/* <span className="svg-icon svg-icon-xl ">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Layout/Layout-4-blocks.svg"
                  )}
                />
              </span> */}
              <div
              className="btn btn-sm btn-icon btn-bg-warning btn-text-warning btn-hover-warning"
              id="kt_quick_panel_toggle"
            >
              <i className="flaticon2-fast-next text-white"></i>
            </div>
            {/* </div> */}
          </div>
        </OverlayTrigger>
        {/* <OverlayTrigger
          placement="left"
          overlay={<Tooltip id="layout-tooltip">Layout Builder</Tooltip>}
        >
          <li className="nav-item mb-2" data-placement="left">
            <Link
              to="/builder"
              className="btn btn-sm btn-icon btn-bg-light btn-text-primary btn-hover-primary"
            >
              <i className="flaticon2-gear"></i>
            </Link>
          </li>
        </OverlayTrigger> */}

        {/* <OverlayTrigger
          placement="left"
          overlay={<Tooltip id="documentations-tooltip">Documentation</Tooltip>}
        >
          <li className="nav-item mb-2" data-placement="left">
            <a
              className="btn btn-sm btn-icon btn-bg-light btn-text-warning btn-hover-warning"
              target="_blank"
              rel="noopener noreferrer"
              href="https://keenthemes.com/metronic/?page=docs&section=react-quick-start"
            >
              <i className="flaticon2-telegram-logo"></i>
            </a>
          </li>
        </OverlayTrigger> */}
      </div>
    </>
  );
}
