import React, { useMemo } from "react";
import { useHtmlClassService } from "../../_core/MetronicLayout";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function Footer() {
  const today = new Date().getFullYear();
  const uiService = useHtmlClassService();

  const layoutProps = useMemo(() => {
    return {
      footerClasses: uiService.getClasses("footer", true),
      footerContainerClasses: uiService.getClasses("footer_container", true)
    };
  }, [uiService]);

  return (
    <>
      <div
        className={`footer py-4 d-flex flex-lg-column  ${layoutProps.footerClasses}`}
        id="kt_footer"
        style={{ backgroundColor: "#212C5F" }}
      >
        <div
          className={`${layoutProps.footerContainerClasses} d-flex flex-column `}
          style={{ textAlign: "center" }}
        >
          {/* <div className="container"> */}
            <div className="row">
              <div className="col-xl-3 col-lg-6 col-md-6">
                <div className="row">
                  <div className="col-xl-4 col-lg-4 col-md-4">
                    <div className="header-logo">
                      <img
                        height="100px"
                        width="120px"
                        style={{ marginTop: "10px" }}
                        src={toAbsoluteUrl(
                          "/media/logos/logo-kemenkeu-warna.png"
                        )}
                        alt="images"
                      />
                    </div>
                  </div>
                  <div className="col-xl-2 col-lg-2 col-md-1">
                    <div
                      className="line"
                      style={{
                        borderRight: "2px solid white",
                        height: "100px",
                        marginTop: "10px"
                      }}
                    ></div>
                  </div>
                  <div className="col-xl-4 col-lg-4 col-md-4" style={{ paddingLeft: "20px" }}>
                    <div className="header-logo">
                      <img
                        height="75"
                        width="150"
                        style={{ marginTop: "30px" }}
                        src={toAbsoluteUrl("/media/logos/djp-white.png")}
                        alt="images"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="col-xl-4 col-lg-6 col-md-6"
                style={{
                  textAlign: "justify",
                  display: "flex",
                  alignItems: "center"
                }}
              >
                {/* <span className="text-white">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley .
                </span> */}
              </div>
              <div className="col-xl-5 col-lg-12 col-md-12" style={{ paddingLeft: "20px" }}>
                <div className="row">
                  <div
                    className="col-xl-4 col-lg-6 col-md-6"
                    style={{
                      textAlign: "left",
                      display: "flex",
                      alignItems: "center"
                    }}
                  >
                    {/* <div className="d-flex flex-column">
                      <span className="text-white">Nilai-Nilai</span>
                      <span className="text-white">Visi-Misi</span>
                      <span className="text-white">Kode Etik</span>
                      <span className="text-white">DJP dalam Angka</span>
                      <span className="text-white">Alamat Kantor</span>
                    </div> */}
                  </div>
                  <div className="col-xl-8 col-lg-6 col-md-6">
                    <div className="row">
                      <div
                        className="col-md-6"
                        style={{
                          textAlign: "left",
                          paddingLeft: "0px",
                          marginLeft: "-20px"
                        }}
                      >
                        <div className="header-logo">
                          <img
                            height="100"
                            width="150"
                            style={{ marginTop: "1px" }}
                            src={toAbsoluteUrl("/media/logos/situspajak2.png")}
                            alt="images"
                          />
                        </div>
                      </div>
                      <div
                        className="col-md-6"
                        style={{ textAlign: "left" }}
                      >
                        <div className="header-logo">
                          <img
                            height="100"
                            width="150"
                            style={{ marginTop: "1px" }}
                            src={toAbsoluteUrl("/media/logos/kringpajak.png")}
                            alt="images"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <span className="text-white">
                        Jalan Gatot Subroto, Kav. 40-42, Jakarta 12190
                      </span>
                    </div>
                    <div className="row">
                      <span className="text-white">
                        Telp: (+62) 21 - 525 0208
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="d-flex mb-9">
              <div className="flex-shrink-0 mr-7 mt-lg-0 mt-3">
              <div className="header-logo">
                  <img height="100px" width="120px" style={{marginTop:'10px'}} src={toAbsoluteUrl("/media/logos/logo-kemenkeu-warna.png")} alt="images" />
                </div>
              </div>
              <div className="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                <div className="line" style={{borderRight:'2px solid white', height:'100px', marginTop:'10px'}}></div>
              </div>
              <div className="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                <div className="header-logo">
                  <img height="75px" width="150px" style={{marginTop:'30px'}} src={toAbsoluteUrl("/media/logos/djp-white.png")} alt="images" />
                </div>
              </div>
            
              <div className="flex-grow-1" style={{textAlign:'justify', display: 'flex', alignItems: 'center'}}>
                <div className="d-flex flex-column flex-grow-1 pr-8">
                <span className="text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</span>
                </div>
              </div>
              <div className="flex-grow-1" style={{textAlign:'left'}}>
                <div className="d-flex flex-column">
                <span className="text-white">Nilai-Nilai</span>
                <span className="text-white">Visi-Misi</span>
                <span className="text-white">Kode Etik</span>
                <span className="text-white">DJP dalam Angka</span>
                <span className="text-white">Alamat Kantor</span>
                </div>
              </div>
              <div className="flex-shrink-0 mr-7 mt-lg-0 mt-3">
              <div className="header-logo">
                  <img height="100px" width="120px" style={{marginTop:'10px'}} src={toAbsoluteUrl("/media/logos/logo-kemenkeu-warna.png")} alt="images" />
                </div>
              </div>
              <div className="flex-shrink-0 mr-7 mt-lg-0 mt-3">
              <div className="header-logo">
                  <img height="100px" width="120px" style={{marginTop:'10px'}} src={toAbsoluteUrl("/media/logos/logo-kemenkeu-warna.png")} alt="images" />
                </div>
              </div>
						</div> */}
            </div>
          {/* </div> */}
        </div>
      </div>
      {/* Second Footer Beginning */}
      <div
        className={`footer py-4 d-flex flex-lg-column  ${layoutProps.footerClasses}`}
        id="kt_footer"
        style={{ backgroundColor: "#070F32" }}
      >
        <div
          className={`${layoutProps.footerContainerClasses} d-flex flex-column `}
          style={{ textAlign: "center" }}
        >
          <div className="text-dark order-2 order-md-1">
            <span className="text-muted font-weight-bold mr-2">
              Copyright &copy; {today.toString()} Direktorat Jenderal Pajak
            </span>
          </div>
        </div>
      </div>
      {/* Second Footer End */}
    </>
  );
}
