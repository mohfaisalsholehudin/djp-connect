FROM node:12-buster as build-stage
WORKDIR /app
COPY . .
# RUN yarn install && yarn build
# RUN yarn add node-sass
RUN yarn install
RUN yarn build

FROM nginx as production-stage
COPY --from=build-stage /app/build /usr/share/nginx/html/
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
RUN sed -i.bak 's/listen\(.*\)80;/listen 8088;/' /etc/nginx/conf.d/default.conf
EXPOSE 8088 
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf

ENTRYPOINT [ "nginx", "-g", "daemon off;" ]
